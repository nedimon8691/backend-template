﻿using Core.DTOModels.BusinessEntityDTOs;
using Core.Interfaces.BusinessProcessesInterfaces.BusinessEntityInterfaces;
using Core.Services.PaginationServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApi.Controllers.BaseCrudController;

namespace WebApi.Controllers.ApiControllers.BusinessEntitiesControllers
{

    [ApiController]
    [Route("BusinessEntity/[action]")]
    [Authorize]
    public class BusinessEntityController : CrudController<BusinessEntityDTO>
    {
        private readonly IBusinessEntityService service;

        public BusinessEntityController(IBusinessEntityService service, IUriService uriService) : base(service, uriService)
        {
            this.service = service;
        }

        [HttpGet]
        public async Task<IActionResult> CheckEntity(int entityId)
        {
            var checker = await service.CheckEntity(entityId);
            if (checker.Succeeded)
                return Ok(checker);
            else
                return BadRequest(checker);
        }
    }
}
