﻿using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.DTOModels.IdentityDTOs;
using Core.DTOModels.UserAdministrationDTOs;
using Core.Interfaces.UserAdministrationInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.ApiControllers.UsersAdminControllers
{
    [ApiController]
    [Route("Administration/[action]")]
    [Authorize]
    public class UserAdminController : ControllerBase
    {
        private readonly IUserAdministrationService _service;

        public UserAdminController(IUserAdministrationService service)
        {
            _service = service;
        }

        #region User Methods
        [HttpGet]
        public async Task<IActionResult> GetUser(string userId)
        {
            var result = await _service.GetUser(userId);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers(bool blocked = false)
        {
            var result = await _service.GetAllUsers(blocked);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddUser(ApplicationUserCreationDTO dto)
        {
            var result = await _service.AddUser(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateUser(ApplicationUserDTO dto)
        {
            var result = await _service.UpdateUser(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> BlockUser([FromBody] BlockUserDTO dto)
        {
            var result = await _service.BlockUser(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }
        [HttpPost]
        public async Task<IActionResult> UnblockUser([FromBody] BlockUserDTO dto)
        {
            var result = await _service.UnblockUser(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetUserRoles([FromQuery] string userId)
        {
            var result = await _service.GetUserRoles(userId);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> SetUserRoles([FromBody] UserRolesDTO dto)
        {
            var result = await _service.SetUserRoles(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }
        #endregion

        #region Role Methods
        [HttpGet]
        public async Task<IActionResult> GetAllRoles(bool blocked = false)
        {
            var result = await _service.GetAllRoles(blocked);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetRolePermissions(string roleId)
        {
            var result = await _service.GetRolePermissions(roleId);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddRole([FromBody] RoleDTO roleDTO)
        {
            var result = await _service.AddRole(roleDTO);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateRole([FromBody] RoleDTO roleDTO)
        {
            var result = await _service.UpdateRole(roleDTO);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveRole([FromBody] RoleRemoveDTO roleRemoveDTO)
        {
            var result = await _service.RemoveRole(roleRemoveDTO);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> AddRoleToUser([FromBody] RoleToUserDTO roleToUserDTO)
        {
            var result = await _service.AddRoleToUser(roleToUserDTO);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> RemoveRoleFromUser([FromBody] RoleToUserDTO roleToUserDTO)
        {
            var result = await _service.RemoveRoleFromUser(roleToUserDTO);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }
        [HttpPost]
        public async Task<IActionResult> BlockRole([FromBody] BlockRoleDTO dto)
        {
            var result = await _service.BlockRole(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> UnblockRole([FromBody] BlockRoleDTO dto)
        {
            var result = await _service.UnBlockRole(dto);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }
        #endregion


        #region Permission Methods 
        [HttpPost]
        public async Task<IActionResult> AddPermissionClaim([FromBody] ApproveDeclinePermissionDTO dto)
        {
            var result = await _service.AddPermissionClaim(dto.roleId, dto.permissionId);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> RemovePermissionClaim([FromBody] ApproveDeclinePermissionDTO dto)
        {
            var result = await _service.AddPermissionClaim(dto.roleId, dto.permissionId);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }
        #endregion

        #region Application methods 
        [HttpGet]
        public async Task<IActionResult> GetAllApplicationMethods()
        {
            var result = await _service.UpdateAndGetAllApplicationMethods();
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }
        #endregion

    }
}
