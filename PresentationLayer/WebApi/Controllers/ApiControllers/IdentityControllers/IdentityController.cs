﻿using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.DTOModels.IdentityDTOs;
using Core.Interfaces.IdentityInterfaces;
using Core.Interfaces.UserContextInterfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.ApiControllers.IdentityControllers
{
    [ApiController]
    [Route("Identity/[action]")]
    [Authorize]
    public class IdentityController : ControllerBase
    {
        private readonly IIdentityService _service;
        private readonly IUserContext _userContext;

        public IdentityController(IIdentityService identityService, IUserContext userContext)
        {
            _service = identityService;
            _userContext = userContext;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginUserRequestDTO applicationUserLoginDTO, CancellationToken cancellationToken)
        {
            var result = await _service.Login(applicationUserLoginDTO, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshRequest request, CancellationToken cancellationToken)
        {
            var result = await _service.RefreshToken(request, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            var result = await _service.Logout(_userContext.CurrentUserId);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Registration([FromBody] ApplicationUserRegistrationDTO userRegistrationDTO)
        {
            var result = await _service.Registration(userRegistrationDTO);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

        [HttpGet]
        public async Task<IActionResult> CheckToken([FromQuery] string securityStamp)
        {
            var result = await _service.CheckToken(securityStamp);
            if (result.Succeeded)
                return Ok(result);
            return BadRequest(result);
        }

    }
}
