﻿using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.Filter;
using Core.Heplers;
using Core.Interfaces;
using Core.Services.PaginationServices;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers.BaseCrudController
{
    [Authorize]
    public class CrudController<T> : ControllerBase
    {
        private readonly ICrudService<T> _service;
        private readonly IUriService _uriService;

        public CrudController(ICrudService<T> service, IUriService uriService)
        {
            _service = service;
            _uriService = uriService;
        }

        [HttpGet("GetAll")]
        public virtual async Task<IActionResult> Get([FromQuery] PaginationFilter filter)
        {
            var route = Request.Path.Value;

            var data = await _service.GetAll(filter);

            if (data.ErrorCode is null)
            {
                var totalRecords = data.TotalRecords;
                var response = PaginationHelper.CreatePagedReponse(data.Data, filter, totalRecords, _uriService, route);
                return Ok(response);
            }

            return BadRequest(data);
        }

        [HttpGet("GetSingle")]
        public virtual async Task<IActionResult> Get([FromQuery] int id)
        {
            var data = await _service.GetById(id);

            if (data.Succeeded)
                return Ok(data);
            else
                return BadRequest(data);
        }

        [HttpPost("Restore")]
        public virtual async Task<IActionResult> Restore([FromBody] ApiRequestDeleteBodyDTO dto)
        {
            var result = await _service.Restore(dto.id);
            if (result.Succeeded)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpPost("Add")]
        public virtual async Task<IActionResult> Add([FromBody] T value)
        {
            var data = await _service.Add(value);
            if (data.Succeeded)
                return Ok(data);
            else
                return BadRequest(data);
        }

        [HttpPost("Update")]
        public virtual async Task<IActionResult> Update([FromBody] T value)
        {
            var data = await _service.Update(value);
            if (data.Succeeded)
                return Ok(data);
            else
                return BadRequest(data);
            //return Ok(await _service.Update(value));
        }

        [HttpDelete("Delete")]
        public virtual async Task<IActionResult> Delete([FromBody] ApiRequestDeleteBodyDTO dto)
        {
            var result = await _service.Delete(dto.id);
            if (result.Succeeded)
                return Ok(result);
            else
                return BadRequest(result);
        }

    }

}
