﻿using BusinessProcesses.Models.UserModels;
using Core.Interfaces.IdentityInterfaces;
using Core.Interfaces.UserContextInterfaces;
using Microsoft.AspNetCore.Identity;
using System.IdentityModel.Tokens.Jwt;

namespace WebApi.Middlewares
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class AuthorizationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IIdentityService _identityService;
        private readonly IUserContext _userContext;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthorizationMiddleware(RequestDelegate next,
                                       IIdentityService identityService,
                                       UserManager<ApplicationUser> userManager,
                                       IUserContext userContext)
        {
            _next = next;
            _identityService = identityService;
            _userContext = userContext;
            _userManager = userManager;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            if (httpContext.Request.Headers.ContainsKey("Authorization") == false)
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return; //_next(httpContext);
            }

            string? token = httpContext.Request.Headers["Authorization"]
                .FirstOrDefault()?
                .Split("Bearer ")[1];

            if (token!.Length < 2)
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return; //_next(httpContext)
            }

            if (string.IsNullOrEmpty(token))
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return; //_next(httpContext)
            }

            var userId = new JwtSecurityTokenHandler().ReadJwtToken(token)
                .Claims.FirstOrDefault(f => f.Type == "id")?.Value;

            var userFromJWT = await _userManager.FindByIdAsync(userId);

            if (userFromJWT is null)
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return;
            }

            if (userFromJWT.Blocked)
            {
                httpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                return;
            }

            var permission = await _identityService.CheckUserPermision(userFromJWT.Id, httpContext.Request.Path);

            if (!permission.Succeeded)
            {
                httpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
                return;
            }
            _userContext.CurrentUserId = userFromJWT.Id;

            // Fall back to the default implementation.
            await _next.Invoke(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class AuthorizationMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthorizationMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthorizationMiddleware>();
        }
    }
}
