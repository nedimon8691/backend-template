using AuditModule.Extensions;
using AutoMapper;
using BackgroundServices.BackGroundSettings;
using BusinessProcesses.Extensions;
using BusinessProcesses.Mapping;
using Core.Models.AppsettingsModels;
using DBPersistence.Extensions;
using MailNotification.Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

//variables
var appSettings = builder.Configuration.Get<Appsettings>();

// Add services to the container.
builder.Services.AddPersistenceLayer(builder.Configuration);
builder.Services.AddBusinessProcessesModule();
builder.Services.AddAuditModule();
builder.Services.AddBackgroundServices(appSettings.ConnectionStrings.BackGroundDbConnection);
builder.Services.AddMailNotification();

AddSwagger(builder.Services);
AddJWT(builder.Services, appSettings);
AddMapper(builder.Services);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    //Generate seed data 
    //using (var scope = app.Services.CreateScope())
    //{
    //    var services = scope.ServiceProvider;

    //    DataSeeder.SeedData();
    //}
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.UseMiddleware<AuthorizationMiddleware>();

app.MapControllers();

app.Run();


///ADDITIONAL FUNCTIONS
static void AddMapper(IServiceCollection services)
{
    var mapperConfig = new MapperConfiguration(mc =>
    {
        mc.AddProfile(new IdentityModelMappingProfile());
        mc.AddProfile(new UserRoleAdministrationProfile());
        mc.AddProfile(new BusinessEntityMappingProfile());
    });
    IMapper mapper = mapperConfig.CreateMapper();
    services.AddSingleton(mapper);
}

static void AddJWT(IServiceCollection services, Appsettings appSettings)
{
    services.AddAuthentication(x =>
    {
        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer(x =>
    {
        x.SaveToken = true;
        x.RequireHttpsMetadata = false;
        x.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appSettings.JwtSettings.AccessTokenSecret)),
            ValidIssuer = appSettings.JwtSettings.Issuer,
            ValidAudience = appSettings.JwtSettings.Audience,
            ClockSkew = TimeSpan.Zero
        };
    });

}

static void AddSwagger(IServiceCollection services)
{
    services.AddSwaggerGen(x =>
    {
        x.SwaggerDoc("v1", new OpenApiInfo
        {
            Title = "Api",
            Version = "v1"
        });
        x.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme,
            new OpenApiSecurityScheme
            {
                Description = "JWT Authorization header using the Bearer scheme (Example: 'Bearer 12345abcdef')",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = JwtBearerDefaults.AuthenticationScheme
            });
        x.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                    Reference = new OpenApiReference
                    {
                        Type = ReferenceType.SecurityScheme,
                        Id = JwtBearerDefaults.AuthenticationScheme
                    }
                },
                Array.Empty<string>()
            }
        });
    });
}
