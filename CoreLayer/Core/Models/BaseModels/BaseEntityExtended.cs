﻿namespace Core.Models.BaseModels
{
    public abstract class BaseEntityExtended : BaseEntity
    {
        public string? Name { get; set; }
        public string? Desc { get; set; }
    }
}
