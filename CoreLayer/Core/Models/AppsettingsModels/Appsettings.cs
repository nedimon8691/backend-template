﻿namespace Core.Models.AppsettingsModels
{
    public class Appsettings
    {
        public ConnectionStrings ConnectionStrings { get; set; }
        public LoggingSettings Logging { get; set; }
        //public LogFolderSettings LogFolderSettings { get; set; }
        public MailNotificationSettings MailNotificationSettings { get; set; }
        public JwtSettings JwtSettings { get; set; }
        public string FileUploaderDirectory { get; set; }
        public string AllowedHosts { get; set; }
    }
}
