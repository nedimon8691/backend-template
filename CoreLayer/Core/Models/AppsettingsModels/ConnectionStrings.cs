﻿namespace Core.Models.AppsettingsModels
{
    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
        public string AuditConnection { get; set; }
        public string BackGroundDbConnection { get; set; }
    }
}
