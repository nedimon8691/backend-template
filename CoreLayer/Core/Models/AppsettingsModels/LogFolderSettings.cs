﻿namespace Core.Models.AppsettingsModels
{
    public class LogFolderSettings
    {
        public string UploaderFolder { get; set; }
        public string Symlink { get; set; }
    }
}
