﻿namespace Core.Models.AppsettingsModels
{
    public class LoggingSettings
    {
        public Dictionary<string, string> LogLevel { get; set; }
    }
}
