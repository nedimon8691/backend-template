﻿using Core.Models.BaseModels;

namespace Core.Models.ApplicationModels
{
    public class ApplicationMethods : BaseEntity
    {
        public string Method { get; set; }
        public string MethodType { get; set; }
        public string Route { get; set; }
        public string Action { get; set; }
        public string ControllerMethod { get; set; }
        public string Module { get; set; }
    }
}
