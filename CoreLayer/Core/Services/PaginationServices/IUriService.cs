﻿using Core.Filter;

namespace Core.Services.PaginationServices
{
    public interface IUriService
    {
        public Uri GetPageUri(PaginationFilter filter, string route);
    }
}
