﻿using Newtonsoft.Json;

namespace Core.Helpers
{
    public static class CompareObjectHelper
    {
        public static bool GetCompare(object existingObject, object updatedObject)
        {
            try
            {
                if (ReferenceEquals(existingObject, updatedObject)) return true;
                if (existingObject == null || updatedObject == null) return false;
                if (existingObject.GetType() != updatedObject.GetType()) return false;

                var existingObjJson = JsonConvert.SerializeObject(existingObject, Formatting.Indented,
                    new JsonSerializerSettings()
                    {
                        Formatting = Formatting.Indented,
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    });

                var updatedObjJson = JsonConvert.SerializeObject(updatedObject, Formatting.Indented,
                    new JsonSerializerSettings()
                    {
                        Formatting = Formatting.Indented,
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    });

                existingObjJson.OrderBy(o => o);
                updatedObjJson.OrderBy(o => o);

                return existingObjJson == updatedObjJson;
            }
            catch// (Exception ex)
            {
                return false;
            }
        }
    }
}
