﻿namespace Core.DTOModels.UserAdministrationDTOs
{
    public class BlockUserDTO
    {
        public string UserId { get; set; }
    }
}
