﻿namespace Core.DTOModels.UserAdministrationDTOs
{
    public class ApplicationUserCreationDTO
    {
        public DateTime? BirthDate { get; set; }
        public string? FirstName { get; set; }
        public string? SecondName { get; set; }
        public string? Email { get; set; }
        public double? CPD { get; set; }
        public double? KPI { get; set; }
        public string? Picture { get; set; }
        public string? Education { get; set; }
        public int? StaffGradeId { get; set; }
        public int? StaffPositionId { get; set; }
        public int? BusinessUnitId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
