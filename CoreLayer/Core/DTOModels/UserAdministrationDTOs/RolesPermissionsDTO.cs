﻿namespace Core.DTOModels.UserAdministrationDTOs
{
    public class RolesPermissionsDTO
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public IEnumerable<Permission> Permissions { get; set; }
    }

    public class Permission
    {
        public int permissionId { get; set; }
    }
}
