﻿namespace Core.DTOModels.UserAdministrationDTOs
{
    //TO-DO change name of the class
    public class ApplicationUserLoginResponseDTO
    {
        public object UserData { get; set; }
        public string UserName { get; set; }
        public IEnumerable<string> UserRoles { get; set; }
    }
}
