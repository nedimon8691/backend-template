﻿namespace Core.DTOModels.UserAdministrationDTOs
{
    public class RoleDTO
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
        public int[] Permissions { get; set; }
    }
    public class RoleToUserDTO
    {
        public string UserId { get; set; }
        public string RoleId { get; set; }
    }
    public class UserRolesDTO
    {
        public string UserId { get; set; }
        public string[] RoleIds { get; set; }
    }
    public class RoleRemoveDTO
    {
        public string RoleId { get; set; }
        public string RoleName { get; set; }
    }
}
