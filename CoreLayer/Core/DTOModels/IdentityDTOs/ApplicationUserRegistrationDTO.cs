﻿namespace Core.DTOModels.IdentityDTOs
{
    public class ApplicationUserRegistrationDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
    }
}
