﻿namespace Core.DTOModels.IdentityDTOs
{
    public class LoginUserRequestDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
