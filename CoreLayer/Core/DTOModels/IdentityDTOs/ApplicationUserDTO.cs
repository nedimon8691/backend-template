﻿using Microsoft.AspNetCore.Identity;

namespace Core.DTOModels.IdentityDTOs
{
    public class ApplicationUserDTO : IdentityUser
    {
        public DateTime? BirthDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? FirstName { get; set; }
        public string? SecondName { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public bool Blocked { get; set; }
        public DateTime? BlockedAt { get; set; }
    }
}
