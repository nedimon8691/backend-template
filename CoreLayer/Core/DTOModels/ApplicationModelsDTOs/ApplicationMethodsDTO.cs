﻿namespace Core.DTOModels.ApplicationModelsDTOs
{
    public class ApplicationMethodsDTO
    {
        public int Id { get; set; }
        public string Method { get; set; }
        public string MethodType { get; set; }
        public string Route { get; set; }
        public string Action { get; set; }
        public string ControllerMethod { get; set; }
        public string Module { get; set; }
    }
}
