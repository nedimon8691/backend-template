﻿using Core.DTOModels.IdentityDTOs;
using Core.Models.BaseModels;

namespace Core.DTOModels.BusinessEntityDTOs
{
    //The example includes all types of data and relationships (1 to 1, 1 to many, many to many)
    public class BusinessEntityDTO : BaseEntityExtended
    {
        public string? Code { get; set; }
        public decimal Cost { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsActive { get; set; }
        public double? Price { get; set; }
        public string BusinessOwnerId { get; set; }
        public virtual ApplicationUserDTO BusinessOwner { get; set; }
        public int? BusinessEntityTypeId { get; set; }
        public virtual BusinessEntityTypeDTO? BusinessEntityType { get; set; }
        public virtual ICollection<BusinessEntityHistoryDTO> BusinessEntityHistory { get; set; }
        public ICollection<BusinessEntityXBusinessEntityAttributesDTO> AssetSubTypeXAssetAttributes { get; set; }


    }
}
