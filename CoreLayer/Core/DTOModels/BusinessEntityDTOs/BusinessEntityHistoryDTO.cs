﻿using Core.DTOModels.IdentityDTOs;
using Core.Models.BaseModels;

namespace Core.DTOModels.BusinessEntityDTOs
{
    public class BusinessEntityHistoryDTO : BaseEntity
    {
        public DateTime ActionDate { get; set; }
        public string ActionType { get; set; }
        public string PerformerId { get; set; }
        public ApplicationUserDTO Performer { get; set; }
        public int AssetId { get; set; }
        public virtual BusinessEntityDTO BusinessEntity { get; set; }
    }
}
