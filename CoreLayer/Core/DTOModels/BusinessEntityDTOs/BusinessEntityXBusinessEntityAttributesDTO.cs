﻿using Core.Models.BaseModels;

namespace Core.DTOModels.BusinessEntityDTOs
{
    public class BusinessEntityXBusinessEntityAttributesDTO : BaseEntity
    {
        public virtual int BusinessEntityId { get; set; }
        public virtual BusinessEntityDTO BusinessEntity { get; set; }
        public virtual int BusinessEntityAttributesId { get; set; }
        public virtual BusinessEntityAttributesDTO BusinessEntityAttributes { get; set; }
        public int? IntValue { get; set; }
        public double? DoubleValue { get; set; }
        public bool? BoolValue { get; set; }
    }
}
