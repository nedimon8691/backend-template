﻿namespace Core.DTOModels.AuditModuleDTOs
{
    public class LogFileDTO
    {
        public string AuditType { get; set; }
        public string ErrorMessage { get; set; }
        public string FileName { get; set; }
    }
}
