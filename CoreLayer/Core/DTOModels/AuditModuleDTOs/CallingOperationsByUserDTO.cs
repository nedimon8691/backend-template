﻿namespace Core.DTOModels.AuditModuleDTOs
{
    public class CallingOperationsByUserDTO
    {
        public string ClassOrControllerName { get; set; }
        public string MethodName { get; set; }
        public string UserId { get; set; }
        public string Action { get; set; }
        public string Result { get; set; }
    }
}
