﻿namespace Core.DTOModels.AuditModuleDTOs
{
    public class SystemErrorDTO
    {
        public string ClassOrControllerName { get; set; }
        public string MethodName { get; set; }
        public Exception Exception { get; set; }
    }
}
