﻿
namespace Core.DTOModels.BackgroundServicesDTO
{
    public enum IntervalEnum
    {
        Minute,
        Hour,
        Day,
        Week,
        Month
    }
}
