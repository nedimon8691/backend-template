﻿namespace Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO
{
    public class APIResponse<T> : IAPIResponse<T>
    {
        public string? Message { get; set; }
        public bool Succeeded { get; set; }
        public T Data { get; set; }
        public string? ErrorCode { get; set; }

        #region Non Async Methods 

        #region Success Methods 

        public static APIResponse<T> Success()
        {
            return new APIResponse<T>
            {
                Succeeded = true
            };
        }

        public static APIResponse<T> Success(string message)
        {
            return new APIResponse<T>
            {
                Succeeded = true,
                Message = message
            };
        }

        public static APIResponse<T> Success(T data)
        {
            return new APIResponse<T>
            {
                Succeeded = true,
                Data = data
            };
        }

        public static APIResponse<T> Success(T data, string message)
        {
            return new APIResponse<T>
            {
                Succeeded = true,
                Data = data,
                Message = message
            };
        }

        #endregion

        #region Failure Methods 

        public static APIResponse<T> Failure()
        {
            return new APIResponse<T>
            {
                Succeeded = false
            };
        }


        public static APIResponse<T> Failure(string message)
        {
            return new APIResponse<T>
            {
                Succeeded = false,
                Message = message
            };
        }

        public static APIResponse<T> Failure(T data)
        {
            return new APIResponse<T>
            {
                Succeeded = false,
                Data = data
            };
        }

        public static APIResponse<T> Failure(T data, string message)
        {
            return new APIResponse<T>
            {
                Succeeded = false,
                Message = message,
                Data = data
            };
        }

        #endregion

        #endregion

        #region Async Methods 

        #region Success Methods 

        public static Task<APIResponse<T>> SuccessAsync()
        {
            return Task.FromResult(Success());
        }

        public static Task<APIResponse<T>> SuccessAsync(string message)
        {
            return Task.FromResult(Success(message));
        }

        public static Task<APIResponse<T>> SuccessAsync(T data)
        {
            return Task.FromResult(Success(data));
        }

        public static Task<APIResponse<T>> SuccessAsync(T data, string message)
        {
            return Task.FromResult(Success(data, message));
        }

        #endregion

        #region Failure Methods 

        public static Task<APIResponse<T>> FailureAsync()
        {
            return Task.FromResult(Failure());
        }

        public static Task<APIResponse<T>> FailureAsync(string message)
        {
            return Task.FromResult(Failure(message));
        }

        public static Task<APIResponse<T>> FailureAsync(T data)
        {
            return Task.FromResult(Failure(data));
        }

        public static Task<APIResponse<T>> FailureAsync(T data, string message)
        {
            return Task.FromResult(Failure(data, message));
        }

        #endregion

        #endregion
    }
}
