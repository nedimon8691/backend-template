﻿namespace Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO
{
    public interface IAPIResponse<T>
    {
        string? Message { get; set; }
        bool Succeeded { get; set; }
        T Data { get; set; }
        string? ErrorCode { get; set; }
    }
}
