﻿namespace Core.DTOModels.APIRequestResponseDTOModels.RequestDTO
{
    public class RefreshRequest
    {
        /// The refresh token.
        public string RefreshToken { get; set; }
    }
}
