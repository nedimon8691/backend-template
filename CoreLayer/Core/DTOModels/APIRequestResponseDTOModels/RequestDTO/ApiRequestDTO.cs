﻿namespace Core.DTOModels.APIRequestResponseDTOModels.RequestDTO
{
    public class ApiRequestDTO
    {
        public int Id { get; set; }
    }

    public class ApiRequestWithUserIdDTO : ApiRequestDTO
    {
        public string UserId { get; set; }
    }

    public class ApiRequestDeleteBodyDTO
    {
        public int id { get; set; }
    }

    public class ApproveDeclinePermissionDTO
    {
        public string roleId { get; set; }
        public int permissionId { get; set; }
    }
}
