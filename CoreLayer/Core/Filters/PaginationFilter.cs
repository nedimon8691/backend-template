﻿
namespace Core.Filter
{
    public class PaginationFilter
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public bool IsDeleted { get; set; }
        public string Search { get; set; }
        public PaginationFilter()
        {
            PageNumber = 1;
            PageSize = 10;
            IsDeleted = false;
        }
        public PaginationFilter(int pageNumber, int pageSize, bool isDeleted, string search)
        {
            PageNumber = pageNumber < 1 ? 1 : pageNumber;
            PageSize = pageSize < 10 ? 10 : pageSize;
            IsDeleted = isDeleted;
            Search = search;
        }
    }
}
