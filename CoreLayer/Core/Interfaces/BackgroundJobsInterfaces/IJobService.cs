﻿using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.BackgroundServicesDTO;

namespace BackgroundServices.Services
{
    public interface IJobService
    {
        Task<IAPIResponse<List<JobDTO>>> GetJobs(string userName);
        Task<IAPIResponse<JobDTO>> GetJob(int id);
        Task<IAPIResponse<JobDTO>> AddJob(JobRequestDTO job, string userName);
        Task<IAPIResponse<JobDTO>> UpdateJob(JobDTO job, string userName);
        Task<IAPIResponse<bool>> DeleteJob(int id, string userName);
    }
}
