﻿using Core.DTOModels.BackgroundServicesDTO;

namespace BackgroundServices.Services
{
    public interface IJobsRepository
    {
        Task<List<JobDTO>> GetJobs(string userName);
        Task<JobDTO> GetJob(int id);
        Task<JobDTO> AddJob(JobRequestDTO job, string userName);
        Task<JobDTO> UpdateJob(JobDTO job, string userName);
        Task<bool> DeleteJob(int id, string userName);
    }
}
