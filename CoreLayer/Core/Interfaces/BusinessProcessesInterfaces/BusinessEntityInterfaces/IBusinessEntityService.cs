﻿using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.BusinessEntityDTOs;

namespace Core.Interfaces.BusinessProcessesInterfaces.BusinessEntityInterfaces
{
    public interface IBusinessEntityService : ICrudService<BusinessEntityDTO>
    {
        Task<IAPIResponse<bool>> CheckEntity(int entityId);
    }
}
