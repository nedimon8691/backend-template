﻿namespace Core.Interfaces.NotificationsInterfaces
{
    public interface IMailNotificationService
    {
        Task SendEmailAsync(string subject, List<string> receiverEmails, string body, byte[] file);
    }
}
