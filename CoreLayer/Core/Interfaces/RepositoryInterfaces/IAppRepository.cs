﻿namespace Core.Interfaces.RepositoryInterfaces
{
    public interface IAppRepository//<T> where T : class
    {
        Task<T> GetByIdAsync<T>(int id) where T : class;
        Task<List<T>> GetAllAsync<T>() where T : class;
        IQueryable<T> GetAllQuery<T>() where T : class;
        Task<T> AddAsync<T>(T entity) where T : class;
        Task<List<T>> AddRangeAsync<T>(List<T> entities) where T : class;
        void Update<T>(T entity) where T : class;
        Task DeleteFromDBAsync<T>(T entity) where T : class;
        Task DeleteFromDBAsyncRange<T>(IEnumerable<T> entities) where T : class;
        Task SaveChangesAsync();
        Task SaveChangesAsync(CancellationToken cancellationToken);
    }
}
