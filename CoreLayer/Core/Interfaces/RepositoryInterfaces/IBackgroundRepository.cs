﻿namespace Core.Interfaces.RepositoryInterfaces
{
    public interface IBackgroundRepository
    {
        Task<T> AddJob<T>(T job) where T : class;
        void UpdateJob<T>(T job) where T : class;
        Task<T> GetJob<T>(int id) where T : class;
        List<T> GetJobs<T>() where T : class;
        void DeleteJob(int id);
        void SaveChanges();

    }
}
