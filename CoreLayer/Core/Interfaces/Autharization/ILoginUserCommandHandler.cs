﻿using Core.DTOModels.IdentityDTOs;

namespace Core.Interfaces.Authorization
{
    public interface ILoginUserCommandHandler
    {
        Task<ApplicationUserDTO> Handle(LoginUserRequestDTO request, CancellationToken cancellationToken);
    }
}