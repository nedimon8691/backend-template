﻿namespace Core.Interfaces.Authorization
{
    /// For validating refresh token.
    public interface IRefreshTokenValidator
    {
        Task<bool> Validate(string refreshToken);
    }
}