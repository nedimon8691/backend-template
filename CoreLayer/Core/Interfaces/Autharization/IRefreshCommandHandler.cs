﻿using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.DTOModels.IdentityDTOs;

namespace Core.Interfaces.Authorization
{
    public interface IRefreshCommandHandler
    {
        Task<ApplicationUserDTO> Handle(RefreshRequest request, CancellationToken cancellationToken);
    }
}