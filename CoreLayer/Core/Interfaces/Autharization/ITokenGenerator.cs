﻿using System.Security.Claims;

namespace Core.Interfaces.Authorization
{
    public interface ITokenGenerator
    {
        /// Generates jwt token
        string Generate(string secretKey,
                        string issuer,
                        string audience,
                        double expires,
                        IEnumerable<Claim> claims = null);
    }
}