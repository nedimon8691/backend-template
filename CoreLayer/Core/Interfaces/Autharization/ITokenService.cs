﻿namespace Core.Interfaces.Authorization
{
    /// Interface for generating token.
    public interface ITokenService
    {
        Task<string> Generate(string userId);
    }
}
