﻿using Core.Interfaces.Authorization;

namespace Core.Interfaces.Autharization
{
    /// <inheritdoc cref="ITokenService"/>
    public interface IAccessTokenService : ITokenService { }
}