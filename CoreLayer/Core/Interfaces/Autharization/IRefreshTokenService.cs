﻿namespace Core.Interfaces.Authorization
{
    /// <inheritdoc cref="ITokenService"/>
    public interface IRefreshTokenService : ITokenService
    {
    }
}