﻿namespace Core.Interfaces.UserContextInterfaces
{
    public interface IUserContext
    {
        string CurrentUserId { get; set; }
    }
}
