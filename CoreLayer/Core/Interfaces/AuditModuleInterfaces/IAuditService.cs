﻿using Core.DTOModels.AuditModuleDTOs;

namespace Core.Interfaces.AuditModuleInterfaces
{
    public interface IAuditService
    {
        Task CreateSystemErrorAuditAsync(SystemErrorDTO systemError);
        Task CreateCallingOperationsByUserAuditAsync(CallingOperationsByUserDTO auditData);
        void CreateLogFile(LogFileDTO logFile);
        Task CreateChangesAuditTrail(AuditChangesDTO auditData);
    }
}
