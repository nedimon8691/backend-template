﻿namespace Core.Interfaces.AuditModuleInterfaces
{
    public interface IAuditDBRepository
    {
        Task<T> Add<T>(T entity) where T : class;
        Task<T> Update<T>(T entity) where T : class;
        Task<T> Get<T>(int id) where T : class;
        Task<List<T>> GetAll<T>() where T : class;
        void Delete<T>(int id) where T : class;
        void SaveChangesAsync();
    }
}
