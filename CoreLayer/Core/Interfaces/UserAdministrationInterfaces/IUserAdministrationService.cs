﻿using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.ApplicationModelsDTOs;
using Core.DTOModels.IdentityDTOs;
using Core.DTOModels.UserAdministrationDTOs;

namespace Core.Interfaces.UserAdministrationInterfaces
{
    public interface IUserAdministrationService
    {
        #region User Methods
        Task<IAPIResponse<ApplicationUserDTO>> GetUser(string userId);
        Task<IAPIResponse<IEnumerable<ApplicationUserDTO>>> GetAllUsers(bool blocked);
        Task<IAPIResponse<ApplicationUserDTO>> AddUser(ApplicationUserCreationDTO dto);
        Task<IAPIResponse<ApplicationUserDTO>> UpdateUser(ApplicationUserDTO dto);
        Task<IAPIResponse<bool>> BlockUser(BlockUserDTO dto);
        Task<IAPIResponse<bool>> UnblockUser(BlockUserDTO dto);
        Task<IAPIResponse<IEnumerable<RoleDTO>>> GetUserRoles(string userID);
        Task<IAPIResponse<bool>> SetUserRoles(UserRolesDTO dto);
        #endregion

        #region Role Methods
        Task<IAPIResponse<IEnumerable<RoleDTO>>> GetAllRoles(bool blocked);
        Task<IAPIResponse<RolesPermissionsDTO>> GetRolePermissions(string roleId);
        Task<IAPIResponse<RoleDTO>> AddRole(RoleDTO paramRoleDTO);
        Task<IAPIResponse<RoleDTO>> UpdateRole(RoleDTO roleDTO);
        Task<IAPIResponse<RoleDTO>> RemoveRole(RoleRemoveDTO roleRemoveDTO);
        Task<IAPIResponse<ApplicationUserLoginResponseDTO>> AddRoleToUser(RoleToUserDTO roleToUserDTO);
        Task<IAPIResponse<ApplicationUserLoginResponseDTO>> RemoveRoleFromUser(RoleToUserDTO roleToUserDTO);
        Task<IAPIResponse<bool>> BlockRole(BlockRoleDTO dTO);
        Task<IAPIResponse<bool>> UnBlockRole(BlockRoleDTO dto);
        #endregion


        #region Permission Methods 
        Task<IAPIResponse<bool>> AddPermissionClaim(string roleId, int permissionId);
        Task<IAPIResponse<bool>> RemovePermissionClaim(string roleId, string permission);
        #endregion

        #region Application methods 
        Task<IAPIResponse<IEnumerable<ApplicationMethodsDTO>>> UpdateAndGetAllApplicationMethods();
        #endregion

    }
}
