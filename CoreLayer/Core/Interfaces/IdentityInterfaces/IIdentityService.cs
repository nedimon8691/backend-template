﻿using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.IdentityDTOs;

namespace Core.Interfaces.IdentityInterfaces
{
    public interface IIdentityService
    {
        Task<IAPIResponse<ApplicationUserDTO>> Registration(ApplicationUserRegistrationDTO userRegistrationDTO);
        Task<IAPIResponse<ApplicationUserDTO>> Login(LoginUserRequestDTO request, CancellationToken cancellationToken);
        Task<IAPIResponse<bool>> Logout(string userId);
        Task<IAPIResponse<ApplicationUserDTO>> RefreshToken(RefreshRequest request, CancellationToken cancellationToken);
        Task<IAPIResponse<bool>> CheckUserPermision(string userId, string method);
        Task<IAPIResponse<bool>> CheckToken(string accessToken);
        Task<IAPIResponse<string>> GetUserNameById(string userId);
    }
}
