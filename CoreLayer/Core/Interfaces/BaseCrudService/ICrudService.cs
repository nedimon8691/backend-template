﻿using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.Filter;

namespace Core.Interfaces
{
    public interface ICrudService<T>
    {
        Task<IAPIResponse<T>> Add(T entity);
        Task<IAPIResponse<bool>> Delete(int id);
        Task<IAPIResponse<bool>> Restore(int id);
        Task<IAPIResponse<T>> GetById(int id);
        Task<IAPIResponse<T>> Update(T entity);
        Task<PagedResponse<List<T>>> GetAll(PaginationFilter filter);
    }
}
