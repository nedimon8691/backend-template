﻿using AutoMapper;
using BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels;
using BusinessProcesses.Services.BusinessProcessServices.BusinessEntityServices;
using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.BusinessEntityDTOs;
using Core.Helpers;
using Core.Interfaces.BusinessProcessesInterfaces.BusinessEntityInterfaces;
using Core.Interfaces.RepositoryInterfaces;
using Core.Interfaces.UserContextInterfaces;
using DBPersistence.Data.DBContext;
using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionalTests.BusinessProcessesTests.BusinessProcessServicesTests
{
    [TestClass]
    public class BusinessEntityServiceTests
    {

        private Mock<IUserContext> _userContextMock;
        private Mock<IAppRepository> _repositoryMock;
        private Mock<IMapper> _mapperMock;
        private BusinessEntityService _service;


        [TestInitialize]
        public void Setup()
        {
            _userContextMock = new Mock<IUserContext>();
            _repositoryMock = new Mock<IAppRepository>();
            _mapperMock = new Mock<IMapper>();
            _service = new BusinessEntityService(_userContextMock.Object, _repositoryMock.Object, _mapperMock.Object);
        }
        #region Add
        [TestMethod]
        public async Task Add_ShouldReturnSuccessResponse_WhenValidEntityDtoIsPassed()
        {
            // Arrange
            var entityDto = GetBusinessEntityDTO();
            var entity = GetBusinessEntity();

            _mapperMock.Setup(m => m.Map<BusinessEntity>(entityDto)).Returns(entity);
            _repositoryMock.Setup(r => r.AddAsync(It.IsAny<BusinessEntity>())).ReturnsAsync(entity);
            _repositoryMock.Setup(r => r.SaveChangesAsync()).Returns(Task.CompletedTask);
            _mapperMock.Setup(m => m.Map<BusinessEntityDTO>(It.IsAny<BusinessEntity>())).Returns(entityDto);

            //Act
            var result = await _service.Add(entityDto);

            // Assert
            Assert.IsTrue(result.Succeeded);
            _mapperMock.Verify(m => m.Map<BusinessEntity>(entityDto), Times.Once);
            _repositoryMock.Verify(r => r.AddAsync(entity), Times.Once);
            _repositoryMock.Verify(r => r.SaveChangesAsync(), Times.Once);

        }

        [TestMethod]
        public async Task Add_ShouldReturnFailureResponse_WhenExceptionIsThrown()
        {
            // Arrange
            var entityDto = GetBusinessEntityDTO();

            _mapperMock.Setup(m => m.Map<BusinessEntity>(entityDto)).Throws(new Exception());

            // Act
            var result = await _service.Add(entityDto);

            // Assert
            Assert.IsFalse(result.Succeeded);
            Assert.AreEqual("Error was occured", result.Message);
        }
        #endregion

        #region Update
        [TestMethod]
        public async Task Update_ShouldReturnSuccessResponse_WhenEntityDtoHasChanges()
        {
            // Arrange
            var entityDto = GetBusinessEntityDTO();
            var entity = GetBusinessEntity();
            entityDto.Id = 1;
            entity.Id = 1;

            var existingEntityDto = GetBusinessEntityDTO();
            var existingEntityResponse = APIResponse<BusinessEntityDTO>.Success(existingEntityDto);

            _mapperMock.Setup(m => m.Map<BusinessEntity>(entityDto)).Returns(entity);
            //_repositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(existingEntityResponse);
            _repositoryMock.Setup(r => r.Update(entity));
            _repositoryMock.Setup(r => r.SaveChangesAsync()).Returns(Task.CompletedTask);
            _mapperMock.Setup(m => m.Map<BusinessEntityDTO>(entity)).Returns(entityDto);

            //TODO - adjust compare objects 
            //var compareObjectHelperMock = new Mock<CompareObjectHelper>();
            //compareObjectHelperMock.Setup(h => h.GetCompare(It.IsAny<BusinessEntityDTO>(), It.IsAny<BusinessEntityDTO>()))
            //                       .Returns(false);

            _service = new BusinessEntityService(_userContextMock.Object, _repositoryMock.Object, _mapperMock.Object);

            // Act
            var result = await _service.Update(entityDto);

            // Assert
            Assert.IsTrue(result.Succeeded);
            _repositoryMock.Verify(r => r.Update(It.IsAny<BusinessEntity>()), Times.Once);
            _repositoryMock.Verify(r => r.SaveChangesAsync(), Times.Once);
            _mapperMock.Verify(m => m.Map<BusinessEntityDTO>(It.IsAny<BusinessEntity>()), Times.Once);

        }

        [TestMethod]
        public async Task Update_ShouldReturnSameEntityDto_WhenNoChangesDetected()
        {
            // Arrange
            var entityDto = GetBusinessEntityDTO();
            var entity = GetBusinessEntity();
            entityDto.Id = 1;
            entity.Id = 1;

            var existingEntityDto = entityDto;
            var existingEntityResponse = APIResponse<BusinessEntityDTO>.Success(existingEntityDto);

            _mapperMock.Setup(m => m.Map<BusinessEntity>(entityDto)).Returns(entity);
            //_repositoryMock.Setup(r => r.GetByIdAsync(It.IsAny<int>())).ReturnsAsync(existingEntityResponse);
            _mapperMock.Setup(m => m.Map<BusinessEntityDTO>(entity)).Returns(entityDto);

            //TODO - adjust compare objects 
            //var compareObjectHelperMock = new Mock<CompareObjectHelper>();
            //compareObjectHelperMock.Setup(h => h.GetCompare(It.IsAny<BusinessEntityDTO>(), It.IsAny<BusinessEntityDTO>()))
            //                       .Returns(true);

            _service = new BusinessEntityService(_userContextMock.Object, _repositoryMock.Object, _mapperMock.Object);

            // Act
            var result = await _service.Update(entityDto);

            // Assert
            Assert.IsTrue(result.Succeeded);
            Assert.AreEqual(entityDto, result.Data);
            _repositoryMock.Verify(r => r.Update(It.IsAny<BusinessEntity>()), Times.Never);
            _repositoryMock.Verify(r => r.SaveChangesAsync(), Times.Never);
        }
        #endregion

        public BusinessEntityDTO GetBusinessEntityDTO()
        {
            Random random = new Random();
            return new BusinessEntityDTO
            {
                Id = 0,
                Code = "Test",
                Cost = (decimal)random.NextDouble(),
                CreateDate = DateTime.Now,
                IsActive = true,
                Name = "Test",
                Desc = "Test",
                Price = random.NextDouble(),
                UpdateDate = DateTime.Now,
                BusinessEntityTypeId = null,
                BusinessOwnerId = null,
                AssetSubTypeXAssetAttributes = null,
                BusinessEntityHistory = new List<BusinessEntityHistoryDTO>(),
            };
        }

        public BusinessEntity GetBusinessEntity()
        {
            Random random = new Random();
            return new BusinessEntity
            {
                Id = 0,
                Code = "Test",
                Cost = (decimal)random.NextDouble(),
                CreateDate = DateTime.Now,
                IsActive = true,
                Name = "Test",
                Desc = "Test",
                Price = random.NextDouble(),
                UpdateDate = DateTime.Now,
                BusinessEntityTypeId = null,
                BusinessOwnerId = null,
                AssetSubTypeXAssetAttributes = null,
                BusinessEntityHistory = new List<BusinessEntityHistory>(),
            };
        }
    }

}
