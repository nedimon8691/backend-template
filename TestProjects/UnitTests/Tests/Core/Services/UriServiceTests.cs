﻿using Core.Filter;
using Core.Services.PaginationServices;
using Microsoft.AspNetCore.WebUtilities;

namespace UnitTests.Tests.Core.UriServiceT
{
    [TestClass]
    public class UriServiceTests
    {
        private string _baseUri;
        private UriService _uriService;
        private readonly string ROUTE = "testRoute";
        private readonly string PAGE_NUMBER = "pageNumber";
        private readonly string PAGE_SIZE = "pageSize";
        private readonly string IS_DELETED = "isDeleted";
        private readonly string SEARCH_TERM = "searchTerm";
        private readonly string SEARCH = "search";

        [TestInitialize]
        public void TestInitialize()
        {
            _baseUri = "http://example.com/";
            _uriService = new UriService(_baseUri);
        }

        [TestMethod]
        public void GetPageUri_ValidInput_ReturnsCorrectUri()
        {
            // Arrange
            var filter = new PaginationFilter(2, 10, true, SEARCH_TERM);


            var expectedUri = QueryHelpers.AddQueryString(new Uri(new Uri(_baseUri), ROUTE).ToString(), PAGE_NUMBER, filter.PageNumber.ToString());
            expectedUri = QueryHelpers.AddQueryString(expectedUri, PAGE_SIZE, filter.PageSize.ToString());
            expectedUri = QueryHelpers.AddQueryString(expectedUri, IS_DELETED, filter.IsDeleted.ToString());
            //expectedUri = QueryHelpers.AddQueryString(expectedUri, SEARCH, filter.Search); // Added "search" parameter for completeness

            var expectedUriResult = new Uri(expectedUri);

            // Act
            var result = _uriService.GetPageUri(filter, ROUTE);

            // Assert
            Assert.AreEqual(expectedUriResult.AbsoluteUri, result.AbsoluteUri);
        }

        [TestMethod]
        public void GetPageUri_EmptyFilter_ReturnsCorrectUri()
        {
            // Arrange
            var filter = new PaginationFilter(1, 10, false, string.Empty);

            var expectedUri = QueryHelpers.AddQueryString(new Uri(new Uri(_baseUri), ROUTE).ToString(), PAGE_NUMBER, filter.PageNumber.ToString());
            expectedUri = QueryHelpers.AddQueryString(expectedUri, PAGE_SIZE, filter.PageSize.ToString());
            expectedUri = QueryHelpers.AddQueryString(expectedUri, IS_DELETED, filter.IsDeleted.ToString());

            var expectedUriResult = new Uri(expectedUri);

            // Act
            var result = _uriService.GetPageUri(filter, ROUTE);

            // Assert
            Assert.AreEqual(expectedUriResult, result);
        }
    }

}
