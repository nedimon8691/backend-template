﻿using Core.Filter;
using Core.Heplers;
using Core.Services.PaginationServices;
using Microsoft.AspNetCore.WebUtilities;
using Moq;

namespace UnitTests.Tests.Core.PagedResponse
{

    [TestClass]
    public class PagedResponseTests
    {
        private Mock<IUriService> _mockUriService;
        private readonly string ROUTE = "testRoute";
        private readonly string PAGE_NUMBER = "pageNumber";
        private readonly string PAGE_SIZE = "pageSize";
        private readonly string IS_DELETED = "isDeleted";

        [TestInitialize]
        public void TestInitialize()
        {
            // Set up the mock UriService
            _mockUriService = new Mock<IUriService>();

            // Configure the mock to return a specific URI format
            _mockUriService.Setup(service => service.GetPageUri(It.IsAny<PaginationFilter>(), It.IsAny<string>()))
                .Returns((PaginationFilter filter, string route) =>
                {
                    var baseUri = "http://example.com/";
                    var endpointUri = new Uri(new Uri(baseUri), route);

                    var modifiedUri = QueryHelpers.AddQueryString(endpointUri.ToString(), PAGE_NUMBER, filter.PageNumber.ToString());
                    modifiedUri = QueryHelpers.AddQueryString(modifiedUri, PAGE_SIZE, filter.PageSize.ToString());
                    modifiedUri = QueryHelpers.AddQueryString(modifiedUri, IS_DELETED, filter.IsDeleted.ToString());

                    return new Uri(modifiedUri);
                });
        }

        [TestMethod]
        public void CreatePagedReponse_ValidInput_ReturnsCorrectPagedResponse()
        {
            // Arrange
            var pagedData = new List<int> { 1, 2, 3 };
            var validFilter = new PaginationFilter(1, 10, false, string.Empty);
            int totalRecords = 30;

            // Act
            var response = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _mockUriService.Object, ROUTE);

            // Assert
            //Assert.AreEqual(1, response.CurrentPage);
            Assert.AreEqual(10, response.PageSize);
            Assert.AreEqual(3, response.TotalPages);
            Assert.AreEqual(totalRecords, response.TotalRecords);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=2&pageSize=10&isDeleted=False", response.NextPage.AbsoluteUri);
            Assert.AreEqual(null, response.PreviousPage);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=1&pageSize=10&isDeleted=False", response.FirstPage.AbsoluteUri);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=3&pageSize=10&isDeleted=False", response.LastPage.AbsoluteUri);
        }

        [TestMethod]
        public void CreatePagedReponse_LastPageNoNextPage_ReturnsNullForNextPage()
        {
            // Arrange
            var pagedData = new List<int> { 1, 2, 3 };
            var validFilter = new PaginationFilter(3, 10, false, string.Empty);
            int totalRecords = 30;

            // Act
            var response = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _mockUriService.Object, ROUTE);

            // Assert
            Assert.IsNull(response.NextPage);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=2&pageSize=10&isDeleted=False", response.PreviousPage.AbsoluteUri);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=1&pageSize=10&isDeleted=False", response.FirstPage.AbsoluteUri);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=3&pageSize=10&isDeleted=False", response.LastPage.AbsoluteUri);
        }

        [TestMethod]
        public void CreatePagedReponse_EmptyPagedData_ReturnsCorrectPagedResponse()
        {
            // Arrange
            var pagedData = new List<int>();
            var validFilter = new PaginationFilter(1, 10, false, string.Empty);
            int totalRecords = 0;

            // Act
            var response = PaginationHelper.CreatePagedReponse(pagedData, validFilter, totalRecords, _mockUriService.Object, ROUTE);

            // Assert
            Assert.AreEqual(0, response.TotalPages);
            Assert.AreEqual(totalRecords, response.TotalRecords);
            Assert.IsNull(response.NextPage);
            Assert.IsNull(response.PreviousPage);
            Assert.AreEqual("http://example.com/testRoute?pageNumber=1&pageSize=10&isDeleted=False", response.FirstPage.AbsoluteUri);
            //Assert.IsNull(response.LastPage);
        }
    }

}
