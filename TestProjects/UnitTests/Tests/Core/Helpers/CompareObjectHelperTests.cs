﻿using Core.Helpers;

namespace UnitTests.Tests.Core.CompareObjectHelperT
{
    [TestClass]
    public class CompareObjectHelperTests
    {
        [TestMethod]
        public void GetCompare_SameObject_ReturnsTrue()
        {
            // Arrange
            var obj = new { Name = "Test", Age = 30 };

            // Act
            bool result = CompareObjectHelper.GetCompare(obj, obj);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetCompare_DifferentObjects_ReturnsFalse()
        {
            // Arrange
            var obj1 = new { Name = "Test", Age = 30 };
            var obj2 = new { Name = "Test", Age = 31 };

            // Act
            bool result = CompareObjectHelper.GetCompare(obj1, obj2);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetCompare_OneNullObject_ReturnsFalse()
        {
            // Arrange
            var obj = new { Name = "Test", Age = 30 };

            // Act
            bool result = CompareObjectHelper.GetCompare(obj, null);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetCompare_BothNullObjects_ReturnsTrue()
        {
            // Act
            bool result = CompareObjectHelper.GetCompare(null, null);

            // Assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GetCompare_DifferentTypes_ReturnsFalse()
        {
            // Arrange
            var obj1 = new { Name = "Test", Age = 30 };
            var obj2 = "A string";

            // Act
            bool result = CompareObjectHelper.GetCompare(obj1, obj2);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void GetCompare_SameJsonContentButDifferentOrder_ReturnsTrue()
        {
            // Arrange
            var obj1 = new { Name = "Test", Age = 30 };
            var obj2 = new { Age = 30, Name = "Test" };

            // Act
            bool result = CompareObjectHelper.GetCompare(obj1, obj2);

            // Assert
            Assert.IsTrue(result);
        }
    }
}
