﻿using Core.Models.BaseModels;

namespace UnitTests.Tests.Core.Models.BaseEntityTests
{
    [TestClass]
    public class BaseEntityTests
    {
        [TestMethod]
        public void BaseEntity_IdProperty_ShouldGetAndSetId()
        {
            // Arrange
            var entity = new ConcreteEntity();
            int testId = 123;

            // Act
            entity.Id = testId;

            // Assert
            Assert.AreEqual(testId, entity.Id);
        }

        [TestMethod]
        public void BaseEntityExtended_NameProperty_ShouldGetAndSetName()
        {
            // Arrange
            var entity = new ConcreteEntity();
            string testName = "Test Name";

            // Act
            entity.Name = testName;

            // Assert
            Assert.AreEqual(testName, entity.Name);
        }

        [TestMethod]
        public void BaseEntityExtended_DescProperty_ShouldGetAndSetDesc()
        {
            // Arrange
            var entity = new ConcreteEntity();
            string testDesc = "Test Description";

            // Act
            entity.Desc = testDesc;

            // Assert
            Assert.AreEqual(testDesc, entity.Desc);
        }

        public class ConcreteEntity : BaseEntityExtended
        {
            // Additional properties or methods can be added if needed.
        }
    }
}
