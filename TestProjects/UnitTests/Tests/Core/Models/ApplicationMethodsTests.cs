﻿using Core.Models.ApplicationModels;
using Core.Models.BaseModels;

namespace UnitTests.Tests.Core.Models.ApplicationMethodsTests
{
    [TestClass]
    public class ApplicationMethodsTests
    {
        [TestMethod]
        public void ApplicationMethods_ShouldInheritFrom_BaseEntity()
        {
            // Arrange & Act
            var applicationMethods = new ApplicationMethods();

            // Assert
            Assert.IsInstanceOfType(applicationMethods, typeof(BaseEntity));
        }

        [TestMethod]
        public void ApplicationMethods_ShouldSetAndGetPropertiesCorrectly()
        {
            // Arrange
            var applicationMethods = new ApplicationMethods
            {
                Id = 1,
                Method = "GET",
                MethodType = "HTTP",
                Route = "/api/test",
                Action = "TestAction",
                ControllerMethod = "TestControllerMethod",
                Module = "TestModule"
            };

            // Act & Assert
            Assert.AreEqual(1, applicationMethods.Id);
            Assert.AreEqual("GET", applicationMethods.Method);
            Assert.AreEqual("HTTP", applicationMethods.MethodType);
            Assert.AreEqual("/api/test", applicationMethods.Route);
            Assert.AreEqual("TestAction", applicationMethods.Action);
            Assert.AreEqual("TestControllerMethod", applicationMethods.ControllerMethod);
            Assert.AreEqual("TestModule", applicationMethods.Module);
        }
    }
}
