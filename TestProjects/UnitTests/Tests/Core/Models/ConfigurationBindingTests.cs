﻿using Core.Models.AppsettingsModels;
using Microsoft.Extensions.Configuration;

namespace UnitTests.Tests.Core.Models.ConfigurationBinding
{
    [TestClass]
    public class ConfigurationBindingTests
    {
        private IConfigurationRoot _configuration;

        [TestInitialize]
        public void Setup()
        {
            // Adjust the path as needed based on where the file is copied
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            var configFilePath = Path.Combine(basePath, "appsettings.json");

            _configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)  // Set the base path to the test output directory
                .AddJsonFile(configFilePath)  // Load the copied appsettings.json file
                .Build();
        }

        [TestMethod]
        public void ConnectionStrings_Should_Bind_To_Configuration()
        {
            var connectionStrings = new ConnectionStrings();
            _configuration.GetSection("ConnectionStrings").Bind(connectionStrings);

            Assert.IsNotNull(connectionStrings);
            Assert.IsFalse(string.IsNullOrEmpty(connectionStrings.DefaultConnection));
            Assert.IsFalse(string.IsNullOrEmpty(connectionStrings.AuditConnection));
            Assert.IsFalse(string.IsNullOrEmpty(connectionStrings.BackGroundDbConnection));
        }

        [TestMethod]
        public void JwtSettings_Should_Bind_To_Configuration()
        {
            var jwtSettings = new JwtSettings();
            _configuration.GetSection("JwtSettings").Bind(jwtSettings);

            Assert.IsNotNull(jwtSettings);
            Assert.IsFalse(string.IsNullOrEmpty(jwtSettings.AccessTokenSecret));
            Assert.IsFalse(string.IsNullOrEmpty(jwtSettings.RefreshTokenSecret));
            Assert.IsTrue(jwtSettings.AccessTokenExpirationMinutes > 0);
            Assert.IsTrue(jwtSettings.RefreshTokenExpirationMinutes > 0);
            Assert.IsFalse(string.IsNullOrEmpty(jwtSettings.Issuer));
            Assert.IsFalse(string.IsNullOrEmpty(jwtSettings.Audience));
            Assert.IsFalse(string.IsNullOrEmpty(jwtSettings.RefreshTokenName));
            Assert.IsFalse(string.IsNullOrEmpty(jwtSettings.LoginProvider));
        }

        [TestMethod]
        public void LogFolderSettings_Should_Bind_To_Configuration()
        {
            var logFolderSettings = new LogFolderSettings();
            _configuration.GetSection("LogFolderSettings").Bind(logFolderSettings);

            Assert.IsNotNull(logFolderSettings);
            Assert.IsFalse(string.IsNullOrEmpty(logFolderSettings.UploaderFolder));
            Assert.IsFalse(string.IsNullOrEmpty(logFolderSettings.Symlink));
        }

        [TestMethod]
        public void LoggingSettings_Should_Bind_To_Configuration()
        {
            var loggingSettings = new LoggingSettings();
            _configuration.GetSection("Logging").Bind(loggingSettings);

            Assert.IsNotNull(loggingSettings);
            Assert.IsNotNull(loggingSettings.LogLevel);
            Assert.IsTrue(loggingSettings.LogLevel.Count > 0);
        }

        [TestMethod]
        public void MailNotificationSettings_Should_Bind_To_Configuration()
        {
            var mailNotificationSettings = new MailNotificationSettings();
            _configuration.GetSection("MailNotificationSettings").Bind(mailNotificationSettings);

            Assert.IsNotNull(mailNotificationSettings);
            Assert.IsFalse(string.IsNullOrEmpty(mailNotificationSettings.Server));
            Assert.IsFalse(string.IsNullOrEmpty(mailNotificationSettings.Port));
            //Assert.IsFalse(string.IsNullOrEmpty(mailNotificationSettings.Domain));
            Assert.IsFalse(string.IsNullOrEmpty(mailNotificationSettings.Username));
            Assert.IsFalse(string.IsNullOrEmpty(mailNotificationSettings.Password));
        }
    }
}
