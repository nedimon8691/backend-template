﻿using MailNotification.MailTemplates;

namespace UnitTests.Tests.MailNotificationT
{
    [TestClass]
    public class MailTemplateTests
    {
        [TestMethod]
        public void GetMailBody_ValidMessage_ReturnsCorrectHtml()
        {
            // Arrange
            var message = "This is a test message.";
            var expectedStart = "<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head>";
            var expectedEnd = "</html>";
            var expectedMessagePart = "<p>This is a test message.</p>";

            // Act
            var result = MailTemplate.GetMailBody(message);
            var t = result.EndsWith(expectedEnd);
            // Assert
            Assert.IsTrue(result.StartsWith(expectedStart), "The HTML does not start as expected.");
            Assert.IsTrue(result.Contains(expectedMessagePart), "The HTML does not contain the message.");
            Assert.IsTrue(result.EndsWith(expectedEnd), "The HTML does not end as expected.");
        }
    }
}
