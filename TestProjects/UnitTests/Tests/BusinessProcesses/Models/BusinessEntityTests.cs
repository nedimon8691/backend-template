﻿using BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels;
using BusinessProcesses.Models.UserModels;

namespace UnitTests.Tests.BusinessProcesses.Models
{
    [TestClass]
    public class BusinessEntityTests
    {
        [TestMethod]
        public void BusinessEntity_ShouldSetAndGetProperties()
        {
            // Arrange
            var businessEntity = new BusinessEntity
            {
                Id = 1,
                Name = "Sample Entity",
                Desc = "A sample business entity.",
                Code = "CODE123",
                Cost = 100.50m,
                CreateDate = new DateTime(2024, 8, 26),
                UpdateDate = new DateTime(2024, 8, 27),
                IsDeleted = false,
                DeletionDate = null,
                IsActive = true,
                Price = 150.75,
                BusinessOwnerId = "user1",
                BusinessOwner = new ApplicationUser
                {
                    Id = "user1",
                    UserName = "OwnerUser"
                },
                BusinessEntityTypeId = 2,
                BusinessEntityType = new BusinessEntityType
                {
                    Id = 2,
                    Name = "Entity Type"
                },
                BusinessEntityHistory = new List<BusinessEntityHistory>
            {
                new BusinessEntityHistory
                {
                    Id = 1,
                    ActionDate = new DateTime(2024, 8, 26),
                    ActionType = "Create",
                    PerformerId = "user1",
                    Performer = new ApplicationUser
                    {
                        Id = "user1",
                        UserName = "PerformerUser"
                    },
                    BusinessEntityId = 1
                }
            },
                AssetSubTypeXAssetAttributes = new List<BusinessEntityXBusinessEntityAttributes>
            {
                new BusinessEntityXBusinessEntityAttributes
                {
                    Id = 1,
                    BusinessEntityId = 1,
                    BusinessEntityAttributesId = 1,
                    IntValue = 10,
                    DoubleValue = 20.5,
                    BoolValue = true
                }
            }
            };

            // Act & Assert
            Assert.AreEqual(1, businessEntity.Id);
            Assert.AreEqual("Sample Entity", businessEntity.Name);
            Assert.AreEqual("A sample business entity.", businessEntity.Desc);
            Assert.AreEqual("CODE123", businessEntity.Code);
            Assert.AreEqual(100.50m, businessEntity.Cost);
            Assert.AreEqual(new DateTime(2024, 8, 26), businessEntity.CreateDate);
            Assert.AreEqual(new DateTime(2024, 8, 27), businessEntity.UpdateDate);
            Assert.AreEqual(false, businessEntity.IsDeleted);
            Assert.IsNull(businessEntity.DeletionDate);
            Assert.AreEqual(true, businessEntity.IsActive);
            Assert.AreEqual(150.75, businessEntity.Price);
            Assert.AreEqual("user1", businessEntity.BusinessOwnerId);
            Assert.IsNotNull(businessEntity.BusinessOwner);
            Assert.AreEqual("OwnerUser", businessEntity.BusinessOwner.UserName);
            Assert.AreEqual(2, businessEntity.BusinessEntityTypeId);
            Assert.IsNotNull(businessEntity.BusinessEntityType);
            Assert.AreEqual("Entity Type", businessEntity.BusinessEntityType.Name);
            Assert.IsNotNull(businessEntity.BusinessEntityHistory);
            Assert.AreEqual(1, businessEntity.BusinessEntityHistory.Count);
            Assert.IsNotNull(businessEntity.BusinessEntityHistory.First(f => f.ActionType == "Create"));
            Assert.IsNotNull(businessEntity.AssetSubTypeXAssetAttributes);
            Assert.AreEqual(1, businessEntity.AssetSubTypeXAssetAttributes.Count);
            Assert.IsNotNull(businessEntity.AssetSubTypeXAssetAttributes.First(f => f.IntValue == 10));
            Assert.IsNotNull(businessEntity.AssetSubTypeXAssetAttributes.First(f => f.DoubleValue == 20.5));
            Assert.IsNotNull(businessEntity.AssetSubTypeXAssetAttributes.First(f => f.BoolValue == true));
        }
    }

}
