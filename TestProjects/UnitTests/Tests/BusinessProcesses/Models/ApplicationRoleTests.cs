﻿using BusinessProcesses.Models.UserModels;

namespace UnitTests.Tests.BusinessProcesses.Models
{
    [TestClass]
    public class ApplicationRoleTests
    {
        [TestMethod]
        public void ApplicationRole_ShouldSetAndGetProperties()
        {
            // Arrange
            var applicationRole = new ApplicationRole
            {
                Id = "role1",
                Name = "Admin",
                NormalizedName = "ADMIN",
                ConcurrencyStamp = "stamp1",
                CreatedAt = new DateTime(2024, 8, 26),
                Blocked = true,
                BlockedAt = new DateTime(2024, 8, 26)
            };

            // Act & Assert
            Assert.AreEqual("role1", applicationRole.Id);
            Assert.AreEqual("Admin", applicationRole.Name);
            Assert.AreEqual("ADMIN", applicationRole.NormalizedName);
            Assert.AreEqual("stamp1", applicationRole.ConcurrencyStamp);
            Assert.AreEqual(new DateTime(2024, 8, 26), applicationRole.CreatedAt);
            Assert.AreEqual(true, applicationRole.Blocked);
            Assert.AreEqual(new DateTime(2024, 8, 26), applicationRole.BlockedAt);
        }
    }
}
