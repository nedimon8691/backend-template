﻿using BusinessProcesses.Models.UserModels;

namespace UnitTests.Tests.BusinessProcesses.Models
{
    [TestClass]
    public class ApplicationUserTests
    {
        [TestMethod]
        public void ApplicationUser_ShouldSetAndGetProperties()
        {
            // Arrange
            var applicationUser = new ApplicationUser
            {
                Id = "user1",
                UserName = "testuser",
                BirthDate = new DateTime(1990, 1, 1),
                CreatedAt = new DateTime(2024, 8, 26),
                FirstName = "John",
                SecondName = "Doe",
                Blocked = true,
                BlockedAt = new DateTime(2024, 8, 26)
            };

            // Act & Assert
            Assert.AreEqual("user1", applicationUser.Id);
            Assert.AreEqual("testuser", applicationUser.UserName);
            Assert.AreEqual(new DateTime(1990, 1, 1), applicationUser.BirthDate);
            Assert.AreEqual(new DateTime(2024, 8, 26), applicationUser.CreatedAt);
            Assert.AreEqual("John", applicationUser.FirstName);
            Assert.AreEqual("Doe", applicationUser.SecondName);
            Assert.AreEqual(true, applicationUser.Blocked);
            Assert.AreEqual(new DateTime(2024, 8, 26), applicationUser.BlockedAt);
        }
    }
}
