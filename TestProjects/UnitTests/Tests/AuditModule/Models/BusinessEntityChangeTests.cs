﻿using AuditDBPersistence.Models.ObjectChangesData;

namespace UnitTests.Tests.AuditModule.Models
{
    [TestClass]
    public class BusinessEntityChangeTests
    {
        [TestMethod]
        public void BusinessEntityChange_ShouldSetAndGetProperties()
        {
            // Arrange
            var businessEntityChange = new BusinessEntityChange
            {
                Id = 1,
                UserId = "user123",
                Type = "Update",
                TableName = "Customers",
                DateTime = new DateTime(2024, 8, 26, 14, 0, 0),
                OldValues = "Old value data",
                NewValues = "New value data",
                AffectedColumns = "Column1, Column2",
                PrimaryKey = "PK_123"
            };

            // Act & Assert
            Assert.AreEqual(1, businessEntityChange.Id);
            Assert.AreEqual("user123", businessEntityChange.UserId);
            Assert.AreEqual("Update", businessEntityChange.Type);
            Assert.AreEqual("Customers", businessEntityChange.TableName);
            Assert.AreEqual(new DateTime(2024, 8, 26, 14, 0, 0), businessEntityChange.DateTime);
            Assert.AreEqual("Old value data", businessEntityChange.OldValues);
            Assert.AreEqual("New value data", businessEntityChange.NewValues);
            Assert.AreEqual("Column1, Column2", businessEntityChange.AffectedColumns);
            Assert.AreEqual("PK_123", businessEntityChange.PrimaryKey);
        }
    }
}
