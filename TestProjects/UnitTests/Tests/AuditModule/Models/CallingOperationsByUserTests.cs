﻿using AuditModule.Models.OperationCallingData;

namespace UnitTests.Tests.AuditModule.Models
{
    [TestClass]
    public class CallingOperationsByUserTests
    {
        [TestMethod]
        public void CallingOperationsByUser_ShouldSetAndGetProperties()
        {
            // Arrange
            var callingOperationsByUser = new CallingOperationsByUser
            {
                Id = 1,
                ClassOrControllerName = "MyController",
                MethodName = "MyMethod",
                OperationDate = new DateTime(2024, 8, 26, 14, 0, 0),
                UserId = "user123",
                Action = "Execute",
                Result = "Success"
            };

            // Act & Assert
            Assert.AreEqual(1, callingOperationsByUser.Id);
            Assert.AreEqual("MyController", callingOperationsByUser.ClassOrControllerName);
            Assert.AreEqual("MyMethod", callingOperationsByUser.MethodName);
            Assert.AreEqual(new DateTime(2024, 8, 26, 14, 0, 0), callingOperationsByUser.OperationDate);
            Assert.AreEqual("user123", callingOperationsByUser.UserId);
            Assert.AreEqual("Execute", callingOperationsByUser.Action);
            Assert.AreEqual("Success", callingOperationsByUser.Result);
        }
    }
}
