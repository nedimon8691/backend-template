﻿using AuditModule.Models.ApplicationErrorsData;

namespace UnitTests.Tests.AuditModule.Models
{
    [TestClass]
    public class SystemErrorsAuditDataTests
    {
        [TestMethod]
        public void SystemErrorsAuditData_ShouldSetAndGetProperties()
        {
            // Arrange
            var auditData = new SystemErrorsAuditData
            {
                Id = 1,
                ClassOrControllerName = "TestClass",
                MethodName = "TestMethod",
                OperationDate = DateTime.Now,
                TargetSiteDeclaringTypeName = "TestTargetType",
                StackTrace = "Stack trace here",
                Source = "TestSource",
                Message = "TestMessage",
                InnerExceptionMessage = "TestInnerException",
                Hresult = 1001,
                HelpLink = "TestHelpLink",
                DataValues = "TestDataValues"
            };

            // Act & Assert
            Assert.AreEqual(1, auditData.Id);
            Assert.AreEqual("TestClass", auditData.ClassOrControllerName);
            Assert.AreEqual("TestMethod", auditData.MethodName);
            Assert.AreEqual(auditData.OperationDate.Date, DateTime.Now.Date);
            Assert.AreEqual("TestTargetType", auditData.TargetSiteDeclaringTypeName);
            Assert.AreEqual("Stack trace here", auditData.StackTrace);
            Assert.AreEqual("TestSource", auditData.Source);
            Assert.AreEqual("TestMessage", auditData.Message);
            Assert.AreEqual("TestInnerException", auditData.InnerExceptionMessage);
            Assert.AreEqual(1001, auditData.Hresult);
            Assert.AreEqual("TestHelpLink", auditData.HelpLink);
            Assert.AreEqual("TestDataValues", auditData.DataValues);
        }
    }
}
