﻿using Core.Models.AppsettingsModels;
using MailNotification.MailTemplates;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;

namespace IntegrationTests.Tests.MailServers
{
    [TestClass]
    public class SmtpIntegrationTests
    {
        private IConfigurationRoot _configuration;
        List<string> _recievers = new List<string> { "nedimon8691@gmail.com" };
        private readonly string SUBJECT = "SMTP INTEGRATION TEST";

        [TestInitialize]
        public void Setup()
        {
            // Adjust the path as needed based on where the file is copied
            var basePath = AppDomain.CurrentDomain.BaseDirectory;
            var configFilePath = Path.Combine(basePath, "appsettings.json");

            _configuration = new ConfigurationBuilder()
                .SetBasePath(basePath)  // Set the base path to the test output directory
                .AddJsonFile(configFilePath)  // Load the copied appsettings.json file
                .Build();
        }

        [TestMethod]
        public async Task Test_SmtpServer_Availability()
        {
            var settings = new MailNotificationSettings();
            _configuration.GetSection("MailNotificationSettings").Bind(settings);
            // Arrange
            using var smtpClient = new SmtpClient(settings.Server)
            {
                Credentials = new NetworkCredential(settings.Username, settings.Password),
                Port = Convert.ToInt32(settings.Port) //TODO
            };

            smtpClient.EnableSsl = true;
            using var message = new MailMessage
            {
                From = new MailAddress(settings.Username),
                Subject = SUBJECT,
                Body = MailTemplate.GetMailBody(SUBJECT),
                IsBodyHtml = true,
            };

            foreach (var receiverEmail in _recievers)
            {
                message.To.Add(receiverEmail);
            }


            // Act
            try
            {
                await smtpClient.SendMailAsync(message);

            }
            catch (Exception ex)
            {
                // Assert
                Assert.Fail($"SMTP server test failed: {ex.Message}");
            }
        }
    }
}

