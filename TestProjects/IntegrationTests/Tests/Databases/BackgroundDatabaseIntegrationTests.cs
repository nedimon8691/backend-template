﻿using DBPersistence.Data.DBContext;
using Microsoft.EntityFrameworkCore;

namespace IntegrationTests.Tests.Databases
{
    [TestClass]
    public class BackgroundDatabaseIntegrationTests
    {
        private DbContextOptions<BackgroundDbContext> _bgDbContext;

        [TestInitialize]
        public void Initialize()
        {
            _bgDbContext = new DbContextOptionsBuilder<BackgroundDbContext>()
                .UseInMemoryDatabase("AppTestDatabase")
                .Options;
        }

        [TestMethod]
        public async Task Test_DatabaseConnection_Success()
        {
            // Arrange
            using (var context = new BackgroundDbContext(_bgDbContext))
            {
                // Act
                var canConnect = await context.Database.CanConnectAsync();

                // Assert
                Assert.IsTrue(canConnect, "Database connection failed.");
            }
        }
    }
}
