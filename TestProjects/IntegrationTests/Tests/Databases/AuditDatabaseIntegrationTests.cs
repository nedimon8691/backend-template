﻿using DBPersistence.Data.DBContext;
using Microsoft.EntityFrameworkCore;

namespace IntegrationTests.Tests.Databases
{
    [TestClass]
    public class AuditDatabaseIntegrationTests
    {
        private DbContextOptions<AuditDBContext> _audDbContext;

        [TestInitialize]
        public void Initialize()
        {
            _audDbContext = new DbContextOptionsBuilder<AuditDBContext>()
                .UseInMemoryDatabase("AuditTestDatabase")
                .Options;
        }

        [TestMethod]
        public async Task Test_DatabaseConnection_Success()
        {
            // Arrange
            using (var context = new AuditDBContext(_audDbContext))
            {
                // Act
                var canConnect = await context.Database.CanConnectAsync();

                // Assert
                Assert.IsTrue(canConnect, "Database connection failed.");
            }
        }
    }
}
