﻿using DBPersistence.Data.DBContext;
using Microsoft.EntityFrameworkCore;

namespace IntegrationTests.Tests.Databases
{
    [TestClass]
    public class AppDatabaseIntegrationTests
    {
        private DbContextOptions<AppDbContext> _dbContext;

        [TestInitialize]
        public void Initialize()
        {
            _dbContext = new DbContextOptionsBuilder<AppDbContext>()
                .UseInMemoryDatabase("AppTestDatabase")
                .Options;
        }

        [TestMethod]
        public async Task Test_DatabaseConnection_Success()
        {
            // Arrange
            using (var context = new AppDbContext(_dbContext))
            {
                // Act
                var canConnect = await context.Database.CanConnectAsync();

                // Assert
                Assert.IsTrue(canConnect, "Database connection failed.");
            }
        }
    }
}
