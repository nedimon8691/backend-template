﻿using AuditModule.Services;
using Core.Interfaces.AuditModuleInterfaces;
using Microsoft.Extensions.DependencyInjection;

namespace AuditModule.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static void AddAuditModule(this IServiceCollection services)
        {
            AddServices(services);
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddTransient<IAuditService, AuditService>();
        }
    }
}
