﻿using Core.Models.BaseModels;

namespace AuditModule.Models.ApplicationErrorsData
{
    public class SystemErrorsAuditData : BaseEntity
    {
        public string ClassOrControllerName { get; set; }
        public string MethodName { get; set; }
        public DateTime OperationDate { get; set; }
        public string TargetSiteDeclaringTypeName { get; set; }
        public string StackTrace { get; set; }
        public string Source { get; set; }
        public string Message { get; set; }
        public string InnerExceptionMessage { get; set; }
        public int Hresult { get; set; }
        public string HelpLink { get; set; }
        public string DataValues { get; set; }
    }
}
