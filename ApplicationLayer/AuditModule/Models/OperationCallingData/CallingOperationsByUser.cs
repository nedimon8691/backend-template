﻿using Core.Models.BaseModels;

namespace AuditModule.Models.OperationCallingData
{
    public class CallingOperationsByUser : BaseEntity
    {
        public string ClassOrControllerName { get; set; }
        public string MethodName { get; set; }
        public DateTime OperationDate { get; set; }
        public string UserId { get; set; }
        public string Action { get; set; }
        public string Result { get; set; }

    }
}
