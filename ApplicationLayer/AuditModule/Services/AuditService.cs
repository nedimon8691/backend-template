﻿using AuditDBPersistence.Models.ObjectChangesData;
using AuditModule.Models.ApplicationErrorsData;
using AuditModule.Models.OperationCallingData;
using Core.DTOModels.AuditModuleDTOs;
using Core.Interfaces.AuditModuleInterfaces;
using Core.Models.AppsettingsModels;
using Microsoft.Extensions.Configuration;

namespace AuditModule.Services
{
    public class AuditService : IAuditService
    {
        private readonly IAuditDBRepository _repository;
        public IConfiguration _configuration;
        private readonly string TEXT_SERVICE_NAME = "AuditServices";

        public AuditService(IAuditDBRepository repository,
                            IConfiguration configuration)
        {
            _repository = repository;
            _configuration = configuration;
        }

        public async Task CreateCallingOperationsByUserAuditAsync(CallingOperationsByUserDTO auditData)
        {
            try
            {
                var newAuditData = new CallingOperationsByUser();
                newAuditData.ClassOrControllerName = auditData.ClassOrControllerName;
                newAuditData.MethodName = auditData.MethodName;
                newAuditData.OperationDate = DateTime.Now;
                newAuditData.UserId = auditData.UserId;
                newAuditData.Action = auditData.Action;
                newAuditData.Result = auditData.Result;

                await _repository.Add(newAuditData);
                _repository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var error = new SystemErrorDTO()
                {
                    ClassOrControllerName = TEXT_SERVICE_NAME,
                    MethodName = "CreateSystemAuditAsync",
                    Exception = ex
                };
                await CreateSystemErrorAuditAsync(error);
            }
        }

        public async Task CreateChangesAuditTrail(AuditChangesDTO auditData)
        {
            try
            {
                var newAuditData = new BusinessEntityChange();
                newAuditData.UserId = auditData.UserId;
                newAuditData.Type = auditData.Type;
                newAuditData.TableName = auditData.TableName;
                newAuditData.DateTime = DateTime.Now;
                newAuditData.OldValues = auditData.OldValues;
                newAuditData.NewValues = auditData.NewValues;
                newAuditData.AffectedColumns = auditData.AffectedColumns;
                newAuditData.PrimaryKey = auditData.PrimaryKey;

                await _repository.Add(newAuditData);
                _repository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var error = new SystemErrorDTO()
                {
                    ClassOrControllerName = TEXT_SERVICE_NAME,
                    MethodName = "CreateChangesAuditTrail",
                    Exception = ex
                };
                await CreateSystemErrorAuditAsync(error);
            }
        }

        public async Task CreateSystemErrorAuditAsync(SystemErrorDTO systemError)
        {
            try
            {
                var newAuditData = new SystemErrorsAuditData()
                {
                    ClassOrControllerName = systemError.ClassOrControllerName,
                    MethodName = systemError.MethodName,
                    OperationDate = DateTime.Now,
                    TargetSiteDeclaringTypeName = systemError.Exception.TargetSite.DeclaringType.Name,
                    StackTrace = systemError.Exception.StackTrace,
                    Source = systemError.Exception.Source,
                    Message = systemError.Exception.Message,
                    InnerExceptionMessage = systemError.Exception.InnerException?.Message,
                    Hresult = systemError.Exception.HResult,
                    HelpLink = systemError.Exception.HelpLink,
                    DataValues = systemError.Exception.Data.Values.ToString()
                };
                await _repository.Add(newAuditData);
                _repository.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var error = new SystemErrorDTO()
                {
                    ClassOrControllerName = TEXT_SERVICE_NAME,
                    MethodName = "CreateSystemErrorAuditAsync",
                    Exception = ex
                };
                await CreateSystemErrorAuditAsync(error);
            }
        }

        public async void CreateLogFile(LogFileDTO _logFile)
        {
            try
            {
                var y = DateTime.Now.Year; var m = DateTime.Now.Month; var d = DateTime.Now.Day; var _dateTimeShort = y + "." + m + "." + d;

                string year = "Logs " + _dateTimeShort;
                var appSettingsSection = _configuration.GetSection("LogFolderSettings");
                var appfolder = appSettingsSection.Get<LogFolderSettings>();
                //var inner = appfolder.Symlink + year + @"/";
                var _rootPath = appfolder.UploaderFolder + year + @"/";

                // var _rootPath = _appEnvironment.WebRootPath + "/Logs/" + _dateTimeShort;
                var _date = DateTime.Now.ToString("MM-dd-yyyy");
                string FileName = _logFile.AuditType + "_" + _date + "_" + _logFile.FileName + "_" + ".txt";
                string FileDirectory = Path.Combine(_rootPath, FileName);

                if (!Directory.Exists(_rootPath))
                {
                    Directory.CreateDirectory(_rootPath);
                }

                if (!File.Exists(FileDirectory))
                {
                    StreamWriter logFile = File.CreateText(System.IO.Path.Combine(_rootPath, FileDirectory));
                    logFile.Close();
                }
                if (File.Exists(FileDirectory))
                {
                    using StreamWriter sw = File.AppendText(FileDirectory);
                    sw.WriteLine(_logFile.ErrorMessage);
                    sw.Close();
                }
            }
            catch (Exception ex)
            {
                var error = new SystemErrorDTO()
                {
                    ClassOrControllerName = TEXT_SERVICE_NAME,
                    MethodName = "CreateLogFile",
                    Exception = ex
                };
                await CreateSystemErrorAuditAsync(error);
            }
        }


    }
}
