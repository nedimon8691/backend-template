﻿using AutoMapper;
using BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels;
using Core.DTOModels.BusinessEntityDTOs;

namespace BusinessProcesses.Mapping
{
    public class BusinessEntityMappingProfile : Profile
    {
        public BusinessEntityMappingProfile()
        {
            CreateMap<BusinessEntityDTO, BusinessEntity>().ReverseMap();
            CreateMap<BusinessEntityTypeDTO, BusinessEntityType>().ReverseMap();
            CreateMap<BusinessEntityHistoryDTO, BusinessEntityHistory>().ReverseMap();
            CreateMap<BusinessEntityAttributesDTO, BusinessEntityAttributes>().ReverseMap();
            CreateMap<BusinessEntityXBusinessEntityAttributesDTO, BusinessEntityXBusinessEntityAttributes>().ReverseMap();
        }
    }
}
