﻿using AutoMapper;
using Core.DTOModels.ApplicationModelsDTOs;
using Core.Models.ApplicationModels;

namespace BusinessProcesses.Mapping
{
    public class UserRoleAdministrationProfile : Profile
    {
        public UserRoleAdministrationProfile()
        {
            CreateMap<ApplicationMethodsDTO, ApplicationMethods>().ReverseMap();
        }
    }
}
