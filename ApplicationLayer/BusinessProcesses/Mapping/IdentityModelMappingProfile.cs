﻿using AutoMapper;
using BusinessProcesses.Models.UserModels;
using Core.DTOModels.IdentityDTOs;
using Core.DTOModels.UserAdministrationDTOs;

namespace BusinessProcesses.Mapping
{
    public class IdentityModelMappingProfile : Profile
    {
        public IdentityModelMappingProfile()
        {
            CreateMap<ApplicationUserDTO, ApplicationUser>().ReverseMap();
            CreateMap<ApplicationUserCreationDTO, ApplicationUserDTO>();
            CreateMap<ApplicationUserCreationDTO, ApplicationUser>();
        }
    }
}
