﻿using BusinessProcesses.Models.UserModels;
using Core.Models.BaseModels;

namespace BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels
{
    public class BusinessEntityHistory : BaseEntity
    {
        public DateTime ActionDate { get; set; }
        public string ActionType { get; set; }
        public string PerformerId { get; set; }
        public ApplicationUser Performer { get; set; }
        public int BusinessEntityId { get; set; }
        public virtual BusinessEntity BusinessEntity { get; set; }
    }
}
