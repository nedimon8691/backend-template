﻿using BusinessProcesses.Models.UserModels;
using Core.Models.BaseModels;

namespace BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels
{
    //The example includes all types of data and relationships (1 to 1, 1 to many, many to many)
    public class BusinessEntity : BaseEntityExtended
    {
        public string? Code { get; set; }
        public decimal Cost { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? UpdateDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletionDate { get; set; }
        public bool IsActive { get; set; }
        public double? Price { get; set; }
        public string BusinessOwnerId { get; set; }
        public virtual ApplicationUser BusinessOwner { get; set; }
        public int? BusinessEntityTypeId { get; set; }
        public virtual BusinessEntityType? BusinessEntityType { get; set; }
        public virtual ICollection<BusinessEntityHistory> BusinessEntityHistory { get; set; }
        public ICollection<BusinessEntityXBusinessEntityAttributes> AssetSubTypeXAssetAttributes { get; set; }


    }
}
