﻿using Core.Models.BaseModels;

namespace BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels
{
    public class BusinessEntityXBusinessEntityAttributes : BaseEntity
    {
        public virtual int BusinessEntityId { get; set; }
        public virtual BusinessEntity BusinessEntity { get; set; }
        public virtual int BusinessEntityAttributesId { get; set; }
        public virtual BusinessEntityAttributes BusinessEntityAttributes { get; set; }
        public int? IntValue { get; set; }
        public double? DoubleValue { get; set; }
        public bool? BoolValue { get; set; }
    }
}
