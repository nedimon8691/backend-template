﻿using Microsoft.AspNetCore.Identity;

namespace BusinessProcesses.Models.UserModels
{
    public class ApplicationUser : IdentityUser
    {
        public DateTime? BirthDate { get; set; }
        public DateTime CreatedAt { get; set; }
        public string? FirstName { get; set; }
        public string? SecondName { get; set; }
        public bool Blocked { get; set; }
        public DateTime? BlockedAt { get; set; }
    }
}
