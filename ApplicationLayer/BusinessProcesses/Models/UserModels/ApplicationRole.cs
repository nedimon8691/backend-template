﻿using Microsoft.AspNetCore.Identity;

namespace BusinessProcesses.Models.UserModels
{
    public class ApplicationRole : IdentityRole
    {
        public DateTime CreatedAt { get; set; }
        public bool Blocked { get; set; }
        public DateTime? BlockedAt { get; set; }
    }
}
