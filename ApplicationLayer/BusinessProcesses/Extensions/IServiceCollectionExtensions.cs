﻿using BusinessProcesses.Services.AuthorisationServices;
using BusinessProcesses.Services.UserContext;
using Core.Interfaces.Autharization;
using Core.Interfaces.Authorization;
using Core.Interfaces.UserContextInterfaces;
using Microsoft.Extensions.DependencyInjection;
using static BusinessProcesses.Services.AuthorisationServices.RefreshCommandHandler;

namespace BusinessProcesses.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static void AddBusinessProcessesModule(this IServiceCollection services)
        {
            services.AddServices();
            services.AddApplicationAuthorization();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IUserContext, UserContext>();
        }

        public static void AddApplicationAuthorization(this IServiceCollection services)
        {
            services.AddScoped<ITokenGenerator, TokenGenerator>();
            services.AddScoped<IAccessTokenService, AccessTokenService>();
            services.AddScoped<IRefreshTokenService, RefreshTokenService>();
            services.AddScoped<IRefreshTokenValidator, RefreshTokenValidator>();
            services.AddScoped<IAccessTokenValidator, AccessTokenValidator>();
            services.AddScoped<IAuthenticateService, AuthenticateService>();

            services.AddScoped<IRefreshCommandHandler, RefreshCommandHandler>();
            services.AddScoped<ILoginUserCommandHandler, LoginUserCommandHandler>();
        }
    }
}
