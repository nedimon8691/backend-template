﻿using AutoMapper;
using BusinessProcesses.Models.UserModels;
using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.IdentityDTOs;
using Core.Interfaces.Authorization;
using Core.Interfaces.IdentityInterfaces;
using Microsoft.AspNetCore.Identity;

namespace BusinessProcesses.Services.IdentityServices
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;
        private IRefreshCommandHandler _refreshCommandHandler;
        private ILoginUserCommandHandler _loginCommandHandler;
        private IAccessTokenValidator _accessTokenValidator;
        private readonly string ERROR_MESSEGE = "Error was occured";
        private readonly string NO_PERMISSION_MESSAGE = "User don't have permission";
        private readonly string REGISTRATION_ERROR_MESSAGE = "Registration details are not complete";
        private readonly string VALID_TOKEN_MESSAGE = "Token is correct";
        private readonly string INVALID_TOKEN_MESSAGE = "Token is not correct";

        public IdentityService(UserManager<ApplicationUser> userManager,
                         RoleManager<ApplicationRole> roleManager,
                         IMapper mapper,
                         IRefreshCommandHandler refreshCommandHandler,
                         ILoginUserCommandHandler loginCommandHandler, IAccessTokenValidator accessTokenValidator)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _refreshCommandHandler = refreshCommandHandler;
            _loginCommandHandler = loginCommandHandler;
            _accessTokenValidator = accessTokenValidator;
        }


        public async Task<IAPIResponse<ApplicationUserDTO>> Registration(ApplicationUserRegistrationDTO userRegistrationDTO)
        {
            try
            {
                if (userRegistrationDTO == null
                    || userRegistrationDTO.Email == null
                    || userRegistrationDTO.Password == null
                    || userRegistrationDTO.UserName == null)
                    return APIResponse<ApplicationUserDTO>.Failure(REGISTRATION_ERROR_MESSAGE);

                var newUser = new ApplicationUser()
                {
                    Email = userRegistrationDTO.Email,
                    UserName = userRegistrationDTO.UserName,
                    CreatedAt = DateTime.Now
                };

                var userCreation = await _userManager.CreateAsync(newUser, userRegistrationDTO.Password);
                if (userCreation.Succeeded)
                {
                    var result = _mapper.Map<ApplicationUserDTO>(newUser);
                    return await APIResponse<ApplicationUserDTO>.SuccessAsync(result);
                }
                else
                    return APIResponse<ApplicationUserDTO>.Failure(ERROR_MESSEGE);
            }
            catch (Exception ex)
            {
                //TODO
                return APIResponse<ApplicationUserDTO>.Failure(ERROR_MESSEGE);
            }

        }

        public async Task<IAPIResponse<ApplicationUserDTO>> Login(LoginUserRequestDTO request, CancellationToken cancellationToken)
        {
            try
            {
                var loginResult = await _loginCommandHandler.Handle(request, cancellationToken);
                return await APIResponse<ApplicationUserDTO>.SuccessAsync(loginResult);
            }
            catch (Exception ex)
            {
                return APIResponse<ApplicationUserDTO>.Failure(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<ApplicationUserDTO>> RefreshToken(RefreshRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var refreshTokenResult = await _refreshCommandHandler.Handle(request, cancellationToken);
                return await APIResponse<ApplicationUserDTO>.SuccessAsync(refreshTokenResult);
            }
            catch (Exception ex)
            {
                return APIResponse<ApplicationUserDTO>.Failure(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> Logout(string userId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                await _userManager.UpdateSecurityStampAsync(user);
                //TO-DO - delete refresh token 
                return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> CheckUserPermision(string userId, string method)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);

                var userRoleNames = await _userManager.GetRolesAsync(user);

                if (userRoleNames.Any())
                {
                    foreach (var roleName in userRoleNames)
                    {
                        var role = await _roleManager.FindByNameAsync(roleName);
                        if (role.Blocked)
                            continue;

                        var claims = await _roleManager.GetClaimsAsync(role);
                        foreach (var claim in claims)
                        {
                            if (claim.Value.ToLower() == method.ToLower())
                            {
                                return await APIResponse<bool>.SuccessAsync();
                            }
                        }
                    }

                    return await APIResponse<bool>.FailureAsync(NO_PERMISSION_MESSAGE);
                }
                else
                    return await APIResponse<bool>.FailureAsync(NO_PERMISSION_MESSAGE);
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> CheckToken(string accessToken)
        {
            try
            {
                var checkTokenResult = _accessTokenValidator.Validate(accessToken);
                if (checkTokenResult)
                    return await APIResponse<bool>.SuccessAsync(VALID_TOKEN_MESSAGE);
                else
                    return await APIResponse<bool>.FailureAsync(INVALID_TOKEN_MESSAGE);
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<string>> GetUserNameById(string userId)
        {
            try
            {
                var userName = (await _userManager.FindByIdAsync(userId)).UserName;
                var response = userName != null ? userName : userId;
                return await APIResponse<string>.SuccessAsync(userName);
            }
            catch (Exception ex)
            {
                return await APIResponse<string>.FailureAsync(ERROR_MESSEGE);
            }
        }

    }
}
