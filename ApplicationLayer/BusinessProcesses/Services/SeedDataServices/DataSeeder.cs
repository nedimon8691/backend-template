﻿using BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels;
using BusinessProcesses.Models.UserModels;
using Core.Interfaces.RepositoryInterfaces;
using Microsoft.AspNetCore.Identity;

namespace BusinessProcesses.Services.SeedDataServices
{
    public static class DataSeeder
    {
        public static readonly IAppRepository _repository;
        private static readonly UserManager<ApplicationUser> _userManager;
        private static readonly RoleManager<ApplicationRole> _roleManager;
        private static readonly string USER_ROLE_ADMIN_TEXT = "Admin";
        private static readonly string EMPLOYEE_ROLE_TEXT = "Employee";
        private static readonly string USER_PASS = "AU!12345d";
        private static readonly string BUSINESS_TYPE_TEXT = "Business Entity Type ";
        private static readonly string ATTRIBUTE_TEXT = "Attribute ";
        private static readonly string BUSINESS_ENTITY_TEXT = "Business Entity ";

        //public staticDataSeeder(IAppRepository repository, 
        //                  UserManager<ApplicationUser> userManager,
        //                  RoleManager<ApplicationRole> roleManager)
        //{
        //    _repository = repository;
        //    _userManager = userManager;
        //    _roleManager = roleManager;
        //}

        public static async void SeedData()
        {
            SeedUserData();
            SeedBusinessData();
        }

        public static async void SeedUserData()
        {
            var users = new List<ApplicationUser>();
            var roles = new List<ApplicationRole>();

            if (!_repository.GetAllQuery<ApplicationUser>().Any() ||
                !_repository.GetAllQuery<ApplicationRole>().Any())
            {
                //new users
                users.Add(new ApplicationUser()
                {
                    UserName = USER_ROLE_ADMIN_TEXT,
                    FirstName = USER_ROLE_ADMIN_TEXT,
                    SecondName = USER_ROLE_ADMIN_TEXT,
                    BirthDate = DateTime.Now,
                    CreatedAt = DateTime.Now,
                    Blocked = false,
                    Email = "admin@mail.com",
                    NormalizedEmail = "ADMIN@MAIL.COM",
                    NormalizedUserName = "ADMIN",
                    PhoneNumber = "444455554444",

                });
                users.Add(new ApplicationUser()
                {
                    UserName = "User1",
                    FirstName = "Tom",
                    SecondName = "Brown",
                    BirthDate = DateTime.Now,
                    CreatedAt = DateTime.Now,
                    Blocked = false,
                    Email = "user1@mail.com",
                    NormalizedEmail = "USER1@MAIL.COM",
                    NormalizedUserName = "TOM BROWN",
                    PhoneNumber = "123456789",

                });
                users.Add(new ApplicationUser()
                {
                    UserName = "User2",
                    FirstName = "Jack",
                    SecondName = "Nugget",
                    BirthDate = DateTime.Now,
                    CreatedAt = DateTime.Now,
                    Blocked = false,
                    Email = "user2@mail.com",
                    NormalizedEmail = "USER2@MAIL.COM",
                    NormalizedUserName = "JACK NUGGET",
                    PhoneNumber = "987654321"
                });

                //new roles
                roles.Add(new ApplicationRole()
                {
                    Name = USER_ROLE_ADMIN_TEXT
                });
                roles.Add(new ApplicationRole()
                {
                    Name = EMPLOYEE_ROLE_TEXT
                });

                //create new users
                foreach (var user in users)
                {
                    await _userManager.CreateAsync(user, USER_PASS);
                }

                //create new roles
                foreach (var role in roles)
                {
                    await _roleManager.CreateAsync(role);
                }

                //Set roles to users
                foreach (var user in users)
                {
                    if (user.UserName == USER_ROLE_ADMIN_TEXT)
                        await _userManager.AddToRoleAsync(user, USER_ROLE_ADMIN_TEXT);
                    else
                        await _userManager.AddToRoleAsync(user, EMPLOYEE_ROLE_TEXT);
                }
            }
        }

        public static async void SeedBusinessData()
        {
            Random random = new Random();
            if (!_repository.GetAllQuery<BusinessEntity>().Any())
            {
                var users = _repository.GetAllQuery<ApplicationUser>().Select(s => s.Id).ToArray();

                var businessEntityTypes = new List<BusinessEntityType>();
                for (int i = 0; i < 4; i++) //3 items
                {
                    businessEntityTypes.Add(new BusinessEntityType()
                    {
                        Name = BUSINESS_TYPE_TEXT + i,
                        Desc = RandomString(30)
                    });
                };
                await _repository.AddRangeAsync(businessEntityTypes);
                await _repository.SaveChangesAsync();

                var businessEntityAttributes = new List<BusinessEntityAttributes>();
                for (int i = 0; i < 6; i++) //5 items
                {
                    businessEntityAttributes.Add(new BusinessEntityAttributes()
                    {
                        Name = ATTRIBUTE_TEXT + i,
                        Desc = RandomString(30)
                    });
                }
                await _repository.AddRangeAsync(businessEntityAttributes);
                await _repository.SaveChangesAsync();

                var businessEntities = new List<BusinessEntity>();
                for (int i = 0; i < 10; i++) //9 items
                {
                    businessEntities.Add(new BusinessEntity()
                    {
                        Name = BUSINESS_ENTITY_TEXT + i,
                        Desc = RandomString(50),
                        Cost = random.Next(1, 999),
                        CreateDate = DateTime.Now,
                        BusinessEntityTypeId = random.Next(1, 3),
                        BusinessOwnerId = users[random.Next(1, 2)],
                        IsActive = true,
                        Code = random.Next(1000, 99999).ToString(),
                        Price = random.Next(1000, 99999),
                        IsDeleted = false
                    });
                }
                var entitiesinDb = await _repository.AddRangeAsync(businessEntities);
                await _repository.SaveChangesAsync();

                var businessEntityXAttributes = new List<BusinessEntityXBusinessEntityAttributes>();
                foreach (var entity in entitiesinDb)
                {

                    for (int i = 0; i < 6; i++)
                    {
                        businessEntityXAttributes.Add(new BusinessEntityXBusinessEntityAttributes()
                        {
                            BoolValue = true,
                            BusinessEntityId = entity.Id,
                            BusinessEntityAttributesId = i,
                            IntValue = random.Next(1, 100),
                            DoubleValue = random.Next(1, 5)
                        });
                    }
                }
                await _repository.AddRangeAsync(businessEntityXAttributes);
                await _repository.SaveChangesAsync();
            }
        }

        public static string RandomString(int length)
        {
            Random random = new Random();
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
