﻿using AutoMapper;
using BusinessProcesses.Models.UserModels;
using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.ApplicationModelsDTOs;
using Core.DTOModels.IdentityDTOs;
using Core.DTOModels.UserAdministrationDTOs;
using Core.Interfaces.RepositoryInterfaces;
using Core.Interfaces.UserAdministrationInterfaces;
using Core.Interfaces.UserContextInterfaces;
using Core.Models.ApplicationModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace BusinessProcesses.Services.UserAdministrationServices
{
    public class UserAdministrationService : IUserAdministrationService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        //private readonly SignInManager<ApplicationUser> _signInManager; 
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly IMapper _mapper;
        private readonly IUserContext _userContext;
        private readonly IEnumerable<EndpointDataSource> _endpointSources;
        private readonly IAppRepository _repository;
        private readonly string ERROR_MESSEGE = "Error was occured";
        private readonly string ERROR_SELF_BLOCK_MESSAGE = "User can not block self";
        private readonly string ERROR_SELF_UNBLOCK_MESSAGE = "User can unblock self";
        private readonly string ERROR_ROLE_EMPTY_NAME_MESSAGE = "Role name is empty";
        private readonly string ERROR_ROLE_EXISTS_MESSAGE = "Role already exists";
        private readonly string ERROR_ROLE_NOT_FOUND_MESSAGE = "Role not found";
        private readonly string ERROR_USER_NOT_FOUND_MESSAGE = "User not found";
        private readonly string ERROR_PERM_OR_ROLE_NOT_FOUND_MESSAGE = "Permission or role not found";

        public UserAdministrationService(UserManager<ApplicationUser> userManager,
                                             //SignInManager<ApplicationUser> signInManager, 
                                             RoleManager<ApplicationRole> roleManager,
                                             IMapper mapper,
                                             IUserContext userContext,
                                             IEnumerable<EndpointDataSource> endpointSources,
                                             IAppRepository repository)
        {
            _userManager = userManager;
            //_signInManager = signInManager;
            _roleManager = roleManager;
            _mapper = mapper;
            _userContext = userContext;
            _endpointSources = endpointSources;
            _repository = repository;
        }

        #region Users Methods
        public async Task<IAPIResponse<ApplicationUserDTO>> GetUser(string userId)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);
                var userDto = _mapper.Map<ApplicationUserDTO>(user);
                return await APIResponse<ApplicationUserDTO>.SuccessAsync(userDto);
            }
            catch (Exception ex)
            {
                return await APIResponse<ApplicationUserDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<IEnumerable<ApplicationUserDTO>>> GetAllUsers(bool blocked)
        {
            try
            {
                var users = await _userManager.Users.ToListAsync();
                users.ForEach(f => { f.PasswordHash = null; f.SecurityStamp = null; });
                var response = _mapper.Map<IEnumerable<ApplicationUserDTO>>(users);
                return await APIResponse<IEnumerable<ApplicationUserDTO>>.SuccessAsync(response);
            }
            catch (Exception ex)
            {
                return await APIResponse<IEnumerable<ApplicationUserDTO>>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<ApplicationUserDTO>> AddUser(ApplicationUserCreationDTO dto)
        {
            try
            {
                var user = _mapper.Map<ApplicationUser>(dto);
                user.CreatedAt = DateTime.Now;

                var result = await _userManager.CreateAsync(user, dto.Password);

                if (!result.Succeeded)
                    return await APIResponse<ApplicationUserDTO>.FailureAsync(ERROR_MESSEGE);
                else
                {
                    var userDto = _mapper.Map<ApplicationUserDTO>(user);
                    return await APIResponse<ApplicationUserDTO>.SuccessAsync(userDto);
                }
            }
            catch (Exception ex)
            {
                return await APIResponse<ApplicationUserDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<ApplicationUserDTO>> UpdateUser(ApplicationUserDTO dto)
        {
            try
            {
                var user = await _userManager.FindByNameAsync(dto.UserName);
                var result = await _userManager.UpdateAsync(user);

                if (!result.Succeeded)
                    return await APIResponse<ApplicationUserDTO>.FailureAsync(ERROR_MESSEGE);
                else
                {
                    var userDto = _mapper.Map<ApplicationUserDTO>(user);
                    return await APIResponse<ApplicationUserDTO>.SuccessAsync(userDto);
                }
            }
            catch (Exception ex)
            {
                return await APIResponse<ApplicationUserDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> BlockUser(BlockUserDTO dto)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(dto.UserId);

                if (user.Id == _userContext.CurrentUserId)
                    throw new ApplicationException(ERROR_SELF_BLOCK_MESSAGE);

                user.Blocked = true;
                user.BlockedAt = DateTime.Now;
                var result = await _userManager.UpdateAsync(user);

                if (!result.Succeeded)
                    return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
                else
                    return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> UnblockUser(BlockUserDTO dto)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(dto.UserId);

                if (user.Id == _userContext.CurrentUserId)
                    throw new ApplicationException(ERROR_SELF_UNBLOCK_MESSAGE);

                user.Blocked = false;
                user.BlockedAt = null;
                var result = await _userManager.UpdateAsync(user);

                if (!result.Succeeded)
                    return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
                else
                    return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<IEnumerable<RoleDTO>>> GetUserRoles(string userID)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userID);

                var response = await _userManager.GetRolesAsync(user);
                var result = new List<RoleDTO>();
                foreach (var roleName in response)
                {
                    var r = await _roleManager.FindByNameAsync(roleName);
                    result.Add(new RoleDTO
                    {
                        RoleId = r.Id,
                        RoleName = r.Name
                    });
                }
                return await APIResponse<IEnumerable<RoleDTO>>.SuccessAsync(result);
            }
            catch (Exception ex)
            {
                return await APIResponse<IEnumerable<RoleDTO>>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> SetUserRoles(UserRolesDTO dto)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(dto.UserId);
                var currentRoles = await _userManager.GetRolesAsync(user);
                var rolesFromDto = dto.RoleIds.Select(roleId => _roleManager.FindByIdAsync(roleId).Result.Name);

                foreach (var role in rolesFromDto.Where(r => currentRoles.Contains(r) == false))
                    await _userManager.AddToRoleAsync(user, role);

                foreach (var role in currentRoles.Where(r => rolesFromDto.Contains(r) == false))
                    await _userManager.RemoveFromRoleAsync(user, role);

                return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        #endregion


        #region Roles Methods
        public async Task<IAPIResponse<IEnumerable<RoleDTO>>> GetAllRoles(bool blocked)
        {
            try
            {
                var response = _roleManager.Roles.Where(r => r.Blocked == blocked);
                var result = response.Select(r => new RoleDTO
                {
                    RoleId = r.Id,
                    RoleName = r.Name,
                }).AsEnumerable();

                return await APIResponse<IEnumerable<RoleDTO>>.SuccessAsync(result);
            }
            catch// (Exception ex)
            {
                return await APIResponse<IEnumerable<RoleDTO>>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<RolesPermissionsDTO>> GetRolePermissions(string roleId)
        {
            try
            {
                var role = await _roleManager.FindByIdAsync(roleId);

                var methods = (await _repository.GetAllAsync<IdentityRoleClaim<string>>())
                              .Where(c => c.RoleId == roleId)
                              .Select(c => c.ClaimValue);

                var perimissions = (await _repository.GetAllAsync<ApplicationMethods>())
                                   .Where(a => methods.Contains(a.Route));

                var rolePermissions = perimissions.Select(e => new Permission()
                {
                    permissionId = e.Id
                });

                var response = new RolesPermissionsDTO()
                {
                    RoleId = roleId,
                    RoleName = role.Name,
                    Permissions = rolePermissions
                };

                return await APIResponse<RolesPermissionsDTO>.SuccessAsync(response);
            }
            catch (Exception ex)
            {
                return await APIResponse<RolesPermissionsDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<RoleDTO>> AddRole(RoleDTO paramRoleDTO)
        {
            try
            {
                var RoleName = paramRoleDTO.RoleName.Trim();

                if (string.IsNullOrWhiteSpace(RoleName))
                    return await APIResponse<RoleDTO>.FailureAsync(ERROR_ROLE_EMPTY_NAME_MESSAGE);

                if (await _roleManager.RoleExistsAsync(RoleName))
                    return await APIResponse<RoleDTO>.FailureAsync(ERROR_ROLE_EXISTS_MESSAGE);

                var role = new ApplicationRole()
                {
                    CreatedAt = DateTime.Now,
                    Name = RoleName
                };
                var response = await _roleManager.CreateAsync(role);

                var routes = (await _repository.GetAllAsync<ApplicationMethods>())
                             .Where(m => paramRoleDTO.Permissions.Contains(m.Id))
                             .Select(s => s.Route).ToArray();

                var claims = routes.Select(m => new Claim(AppClaimTypes.Permission, m));

                foreach (var c in claims)
                    await _roleManager.AddClaimAsync(role, c);

                if (response.Succeeded == false)
                    return await APIResponse<RoleDTO>.FailureAsync(ERROR_MESSEGE);

                paramRoleDTO.RoleId = role.Id;

                return await APIResponse<RoleDTO>.SuccessAsync(paramRoleDTO);
            }
            catch (Exception ex)
            {
                return await APIResponse<RoleDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<RoleDTO>> UpdateRole(RoleDTO roleDTO)
        {
            try
            {
                var role = await _roleManager.FindByIdAsync(roleDTO.RoleId);

                if (role == null)
                    return await APIResponse<RoleDTO>.FailureAsync(ERROR_ROLE_NOT_FOUND_MESSAGE);

                var routes = (await _repository.GetAllAsync<ApplicationMethods>())
                              .Where(m => roleDTO.Permissions.Contains(m.Id))
                              .Select(s => s.Route).ToArray();

                var existingClaims = await _roleManager.GetClaimsAsync(role);

                var claimsToAdd = routes.Except(existingClaims.Select(c => c.Value)).Select(m => new Claim(AppClaimTypes.Permission, m));

                foreach (var c in claimsToAdd)
                    await _roleManager.AddClaimAsync(role, c);

                var claimsToRemove = existingClaims.Where(c => routes.Contains(c.Value) == false);

                foreach (var c in claimsToRemove)
                    await _roleManager.RemoveClaimAsync(role, c);


                var result = await _roleManager.UpdateAsync(role);

                if (result.Succeeded == false)
                    return await APIResponse<RoleDTO>.FailureAsync(ERROR_MESSEGE);

                return await APIResponse<RoleDTO>.SuccessAsync(roleDTO);
            }
            catch (Exception ex)
            {
                return await APIResponse<RoleDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<RoleDTO>> RemoveRole(RoleRemoveDTO roleRemoveDTO)
        {
            try
            {
                var response = await _roleManager.DeleteAsync(await _roleManager.FindByIdAsync(roleRemoveDTO.RoleId) ?? await _roleManager.FindByNameAsync(roleRemoveDTO.RoleName));

                if (!response.Succeeded)
                    return await APIResponse<RoleDTO>.FailureAsync(ERROR_MESSEGE);

                return await APIResponse<RoleDTO>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<RoleDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<ApplicationUserLoginResponseDTO>> AddRoleToUser(RoleToUserDTO roleToUserDTO)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(roleToUserDTO.UserId);
                var role = await _roleManager.FindByIdAsync(roleToUserDTO.RoleId);

                if (user == null)
                    return await APIResponse<ApplicationUserLoginResponseDTO>.FailureAsync(ERROR_USER_NOT_FOUND_MESSAGE);

                if (role == null)
                    return await APIResponse<ApplicationUserLoginResponseDTO>.FailureAsync(ERROR_ROLE_NOT_FOUND_MESSAGE);

                //await _userManager.AddToRoleAsync(user, role.Name);
                await _userManager.AddToRoleAsync(user, role.Name);

                var response = new ApplicationUserLoginResponseDTO()
                {
                    UserName = user.UserName,
                    UserRoles = (await GetUserRoles(user.Id)).Data.Select(r => r.RoleName)
                };
                return await APIResponse<ApplicationUserLoginResponseDTO>.SuccessAsync(response);
            }
            catch (Exception ex)
            {
                return await APIResponse<ApplicationUserLoginResponseDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<ApplicationUserLoginResponseDTO>> RemoveRoleFromUser(RoleToUserDTO roleToUserDTO)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(roleToUserDTO.UserId);
                var role = await _roleManager.FindByIdAsync(roleToUserDTO.RoleId);

                if (user == null)
                    return await APIResponse<ApplicationUserLoginResponseDTO>.FailureAsync(ERROR_USER_NOT_FOUND_MESSAGE);

                if (role == null)
                    return await APIResponse<ApplicationUserLoginResponseDTO>.FailureAsync(ERROR_ROLE_NOT_FOUND_MESSAGE);

                await _userManager.RemoveFromRoleAsync(user, role.Name);
                var response = new ApplicationUserLoginResponseDTO()
                {
                    UserName = user.UserName,
                    UserRoles = (await GetUserRoles(user.Id)).Data.Select(r => r.RoleName)
                };

                return await APIResponse<ApplicationUserLoginResponseDTO>.SuccessAsync(response);
            }
            catch (Exception ex)
            {
                return await APIResponse<ApplicationUserLoginResponseDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> BlockRole(BlockRoleDTO dTO)
        {
            try
            {
                var role = await _roleManager.FindByIdAsync(dTO.RoleId);

                role.Blocked = true;
                role.BlockedAt = DateTime.Now;

                var result = await _roleManager.UpdateAsync(role);

                if (!result.Succeeded)
                    return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
                else
                    return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> UnBlockRole(BlockRoleDTO dto)
        {
            try
            {
                var role = await _roleManager.FindByIdAsync(dto.RoleId);

                role.Blocked = false;
                role.BlockedAt = null;

                var result = await _roleManager.UpdateAsync(role);

                if (!result.Succeeded)
                    return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
                else
                    return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }
        #endregion


        #region Permission Methods 
        public async Task<IAPIResponse<bool>> AddPermissionClaim(string roleId, int permissionId)
        {
            try
            {
                var role = await _roleManager.FindByIdAsync(roleId);
                var permission = await _repository.GetByIdAsync<ApplicationMethods>(permissionId);

                if (permission == null || role == null)
                    return await APIResponse<bool>.FailureAsync(ERROR_PERM_OR_ROLE_NOT_FOUND_MESSAGE);

                var result = await _roleManager.AddClaimAsync(role, new Claim(AppClaimTypes.Permission, permission.Route));
                return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }
        public async Task<IAPIResponse<bool>> RemovePermissionClaim(string roleId, string permission)
        {
            try
            {
                var result = (await _repository.GetAllAsync<IdentityRoleClaim<string>>())
                             .Where(x => x.RoleId == roleId && x.ClaimValue == permission);

                if (result.Count() == 0)
                    return await APIResponse<bool>.FailureAsync(ERROR_PERM_OR_ROLE_NOT_FOUND_MESSAGE);

                await _repository.DeleteFromDBAsyncRange(result);

                await _repository.SaveChangesAsync();

                return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }
        #endregion


        #region Application methods 
        public async Task<IAPIResponse<IEnumerable<ApplicationMethodsDTO>>> UpdateAndGetAllApplicationMethods()
        {
            try
            {
                var methods = (await _repository.GetAllAsync<ApplicationMethods>());
                var realMethods = GetCurrentApplicationMethods();

                var dataToAdd = realMethods.Where(m => methods.Select(m => m.Route)
                .Contains(m.Route) == false).ToList();

                await _repository.AddRangeAsync(dataToAdd);

                await _repository.SaveChangesAsync();

                var response = _mapper.Map<IEnumerable<ApplicationMethodsDTO>>(methods);
                return await APIResponse<IEnumerable<ApplicationMethodsDTO>>.SuccessAsync(response);
            }
            catch// (Exception ex)
            {
                return await APIResponse<IEnumerable<ApplicationMethodsDTO>>.FailureAsync(ERROR_MESSEGE);
            }
        }

        private IEnumerable<ApplicationMethods> GetCurrentApplicationMethods()
        {
            var search = ".Api.";
            var endpoints = _endpointSources
                .SelectMany(es => es.Endpoints)
                .OfType<RouteEndpoint>();
            var output = endpoints.Select(
                e =>
                {
                    var controller = e.Metadata
                        .OfType<ControllerActionDescriptor>()
                        .FirstOrDefault();
                    var action = controller != null
                        ? $"{controller.ControllerName}.{controller.ActionName}"
                        : null;
                    var controllerMethod = controller != null
                        ? $"{controller.ControllerTypeInfo.FullName}:{controller.MethodInfo.Name}"
                        : null;
                    return new ApplicationMethods
                    {
                        Method = e.Metadata.OfType<HttpMethodMetadata>().FirstOrDefault()?.HttpMethods?[0],
                        Route = $"/{e.RoutePattern.RawText.TrimStart('/')}",
                        Action = action,
                        ControllerMethod = controllerMethod,
                        Module = controllerMethod != null ? controllerMethod.Substring(controllerMethod.IndexOf(search) + search.Length).Split('.')[0] : null
                    };
                }
            );
            var applicationMethods = output.Where(w => w.ControllerMethod != null);
            //var gg = t.Count();
            foreach (var item in applicationMethods)
            {
                if (!item.Module.Contains("Manager") || !item.Module.Contains("General"))
                    item.Module = null;
                if (item.Method == "GET") item.MethodType = "View";
                else item.MethodType = "Operation";
            }

            return applicationMethods;
        }
        #endregion





































    }
}
