﻿using Core.Interfaces.UserContextInterfaces;

namespace BusinessProcesses.Services.UserContext
{
    public class UserContext : IUserContext
    {
        public string CurrentUserId { get; set; }
    }
}
