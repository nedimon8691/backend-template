﻿using AutoMapper;
using BusinessProcesses.Models.UserModels;
using Core.DTOModels.APIRequestResponseDTOModels.RequestDTO;
using Core.DTOModels.IdentityDTOs;
using Core.Interfaces.Authorization;
using Core.Interfaces.RepositoryInterfaces;
using Core.Interfaces.UserContextInterfaces;
using Core.Models.AppsettingsModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace BusinessProcesses.Services.AuthorisationServices
{
    public partial class RefreshCommandHandler : IRefreshCommandHandler
    {
        private readonly IAuthenticateService _authenticateService;
        private readonly IRefreshTokenValidator _refreshTokenValidator;
        //private readonly AppDbContext _context;
        private readonly IAppRepository _repository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IUserContext _userContext;
        private readonly IMapper _mapper;
        private readonly Appsettings _appsettings;
        public RefreshCommandHandler(IRefreshTokenValidator refreshTokenValidator,
                                     //AppDbContext context,
                                     IAppRepository repository,
                                     UserManager<ApplicationUser> userManager,
                                     IUserContext userContext,
                                     IAuthenticateService authenticateService,
                                     IMapper mapper,
                                     IOptions<Appsettings> appsettings)
        {
            _refreshTokenValidator = refreshTokenValidator;
            //_context = context;
            _repository = repository;
            _userManager = userManager;
            _userContext = userContext;
            _authenticateService = authenticateService;
            _mapper = mapper;
            _appsettings = appsettings.Value;
        }

        public async Task<ApplicationUserDTO> Handle(RefreshRequest request, CancellationToken cancellationToken)
        {
            var refreshRequest = request;

            var isValidRefreshToken = await _refreshTokenValidator.Validate(refreshRequest.RefreshToken);

            if (!isValidRefreshToken)
                throw new ApplicationException();


            var refreshToken = (await _repository.GetAllAsync<IdentityUserToken<string>>())
                .FirstOrDefault(f => f.Value == request.RefreshToken &&
                                f.LoginProvider == _appsettings.JwtSettings.LoginProvider &&
                                f.Name == _appsettings.JwtSettings.RefreshTokenName);


            if (refreshToken is null)
                throw new ApplicationException();

            var user = await _userManager.FindByIdAsync(refreshToken.UserId);

            if (user is null)
                throw new ApplicationException();

            var isValid = await _userManager.VerifyUserTokenAsync(user, _appsettings.JwtSettings.LoginProvider, _appsettings.JwtSettings.RefreshTokenName, refreshToken.Value);

            await _repository.SaveChangesAsync(cancellationToken);

            var tokens = await _authenticateService.Authenticate(user.Id, cancellationToken);

            var dto = _mapper.Map<ApplicationUserDTO>(user);
            dto.RefreshToken = tokens.RefreshToken;
            dto.AccessToken = tokens.AccessToken;
            return dto;
        }
    }
}