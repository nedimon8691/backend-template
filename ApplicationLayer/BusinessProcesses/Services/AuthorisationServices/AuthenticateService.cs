﻿using AutoMapper;
using BusinessProcesses.Models.UserModels;
using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.Interfaces.Autharization;
using Core.Interfaces.Authorization;
using Core.Interfaces.RepositoryInterfaces;
using Core.Models.AppsettingsModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace BusinessProcesses.Services.AuthorisationServices
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly IAccessTokenService _accessTokenService;
        private readonly IRefreshTokenService _refreshTokenService;
        //private readonly AppDbContext _context;
        private readonly IAppRepository _repository;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly Appsettings _appsettings;
        public AuthenticateService(IAccessTokenService accessTokenService,
                                   IRefreshTokenService refreshTokenService,
                                   IAppRepository repository,
                                   //AppDbContext context, 
                                   IMapper mapper,
                                   UserManager<ApplicationUser> userManager,
                                   IOptions<Appsettings> appsettings)
        {
            _accessTokenService = accessTokenService;
            _refreshTokenService = refreshTokenService;
            _repository = repository;
            //_context = context;
            _mapper = mapper;
            _userManager = userManager;
            _appsettings = appsettings.Value;
        }

        public async Task<AuthenticateResponse> Authenticate(string userId, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(userId);

                var refreshToken = await _refreshTokenService.Generate(userId);

                await _repository.SaveChangesAsync(cancellationToken);

                return new AuthenticateResponse
                {
                    AccessToken = await _accessTokenService.Generate(userId),
                    RefreshToken = refreshToken
                };
            }
            catch//(Exception ex)
            {
                throw;
            }

        }
    }
}