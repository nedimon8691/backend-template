﻿using AutoMapper;
using BusinessProcesses.Models.UserModels;
using Core.DTOModels.IdentityDTOs;
using Core.Interfaces.Authorization;
using Microsoft.AspNetCore.Identity;

namespace BusinessProcesses.Services.AuthorisationServices
{
    public class LoginUserCommandHandler : ILoginUserCommandHandler
    {
        private readonly IAuthenticateService _authenticateService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMapper _mapper;
        private readonly string ERROR_MESSAGE = "Login or password is incorrect";
        public LoginUserCommandHandler(UserManager<ApplicationUser> userManager,
                                       SignInManager<ApplicationUser> signInManager,
                                       IAuthenticateService authenticateService,
                                       IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _authenticateService = authenticateService;
            _mapper = mapper;
        }

        public async Task<ApplicationUserDTO> Handle(LoginUserRequestDTO request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByNameAsync(request.Username);

            if (user is null)
                throw new ApplicationException();

            var check = await _userManager.CheckPasswordAsync(user, request.Password);

            if (!check)
                throw new ApplicationException(ERROR_MESSAGE);

            var tokens = await _authenticateService.Authenticate(user.Id, cancellationToken);

            var dto = _mapper.Map<ApplicationUserDTO>(user);
            dto.PasswordHash = null;
            dto.RefreshToken = tokens.RefreshToken;
            dto.AccessToken = tokens.AccessToken;

            return dto;
        }
    }
}