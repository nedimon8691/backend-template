﻿using BusinessProcesses.Models.UserModels;
using Core.Interfaces.Authorization;
using Core.Models.AppsettingsModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace BusinessProcesses.Services.AuthorisationServices
{
    public partial class RefreshCommandHandler
    {
        public class RefreshTokenService : IRefreshTokenService
        {
            private readonly ITokenGenerator _tokenGenerator;
            private readonly JwtSettings _jwtSettings;
            private readonly UserManager<ApplicationUser> _userManager;
            public RefreshTokenService(ITokenGenerator tokenGenerator,
                                       IOptions<Appsettings> jwtSettings,
                                       UserManager<ApplicationUser> userManager) =>
                (_tokenGenerator, _jwtSettings, _userManager) = (tokenGenerator, jwtSettings.Value.JwtSettings, userManager);

            public async Task<string> Generate(string userId)
            {
                var user = await _userManager.FindByIdAsync(userId);

                if ((await _userManager.GetAuthenticationTokenAsync(user, _jwtSettings.LoginProvider, _jwtSettings.RefreshTokenName)) != null)
                    await _userManager.RemoveAuthenticationTokenAsync(user, _jwtSettings.LoginProvider,
                                                                        _jwtSettings.RefreshTokenName);

                var newRefreshToken = await _userManager.GenerateUserTokenAsync(user,
                            _jwtSettings.LoginProvider, _jwtSettings.RefreshTokenName);

                await _userManager.SetAuthenticationTokenAsync(user, _jwtSettings.LoginProvider,
                                                _jwtSettings.RefreshTokenName, newRefreshToken);

                return newRefreshToken;
            }
        }
    }
}