﻿using BusinessProcesses.Models.UserModels;
using Core.Interfaces.Authorization;
using Core.Interfaces.RepositoryInterfaces;
using Core.Models.AppsettingsModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Text;

namespace BusinessProcesses.Services.AuthorisationServices
{
    public class RefreshTokenValidator : IRefreshTokenValidator
    {
        private readonly JwtSettings _jwtSettings;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAppRepository _repository;
        //private readonly AppDbContext _appDbContext;
        public RefreshTokenValidator(IOptions<Appsettings> jwtSettings,
                                     UserManager<ApplicationUser> userManager,
                                     IAppRepository repository)//, 
                                                               //AppDbContext appDbContext)
        {
            _jwtSettings = jwtSettings.Value.JwtSettings;
            _userManager = userManager;
            _repository = repository;
            //_appDbContext = appDbContext;
        }

        public async Task<bool> Validate(string refreshToken)
        {
            var userId = (await _repository.GetAllAsync<IdentityUserToken<string>>())
                .FirstOrDefault(f => f.Value == refreshToken)?.UserId;

            if (userId == null)
                return false;

            var user = await _userManager.FindByIdAsync(userId);

            return await _userManager.VerifyUserTokenAsync(user, _jwtSettings.LoginProvider, _jwtSettings.RefreshTokenName, refreshToken);
        }
    }
    public class AccessTokenValidator : IAccessTokenValidator
    {
        private readonly JwtSettings _jwtSettings;

        public AccessTokenValidator(IOptions<Appsettings> jwtSettings) => _jwtSettings = jwtSettings.Value.JwtSettings;

        public bool Validate(string refreshToken)
        {
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidateLifetime = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.AccessTokenSecret)),
                ValidIssuer = _jwtSettings.Issuer,
                ValidAudience = _jwtSettings.Audience,
                ClockSkew = TimeSpan.Zero
            };

            JwtSecurityTokenHandler jwtSecurityTokenHandler = new();
            try
            {
                jwtSecurityTokenHandler.ValidateToken(refreshToken,
                                                      validationParameters,
                                                      out SecurityToken validatedToken);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}