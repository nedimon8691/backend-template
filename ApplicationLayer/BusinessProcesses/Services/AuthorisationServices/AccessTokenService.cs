﻿using BusinessProcesses.Models.UserModels;
using Core.Interfaces.Autharization;
using Core.Interfaces.Authorization;
using Core.Models.AppsettingsModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;

namespace BusinessProcesses.Services.AuthorisationServices
{
    public partial class RefreshCommandHandler
    {
        public class AccessTokenService : IAccessTokenService
        {
            private readonly ITokenGenerator _tokenGenerator;
            private readonly JwtSettings _jwtSettings;
            private readonly UserManager<ApplicationUser> _userManager;
            public AccessTokenService(ITokenGenerator tokenGenerator,
                                      IOptions<Appsettings> jwtSettings,
                                      UserManager<ApplicationUser> userManager) =>
                (_tokenGenerator, _jwtSettings, _userManager) = (tokenGenerator, jwtSettings.Value.JwtSettings, userManager);

            public async Task<string> Generate(string userId)
            {
                var user = await _userManager.FindByIdAsync(userId);
                List<Claim> claims = new()
                {
                    new Claim("id", user.Id),
                    new Claim(ClaimTypes.Name, user.UserName),
                };

                return _tokenGenerator.Generate(_jwtSettings.AccessTokenSecret,
                                            _jwtSettings.Issuer, _jwtSettings.Audience,
                                            _jwtSettings.AccessTokenExpirationMinutes, claims);
            }
        }
    }
}