﻿using AutoMapper;
using BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels;
using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.BusinessEntityDTOs;
using Core.Filter;
using Core.Helpers;
using Core.Interfaces.BusinessProcessesInterfaces.BusinessEntityInterfaces;
using Core.Interfaces.IdentityInterfaces;
using Core.Interfaces.RepositoryInterfaces;
using Core.Interfaces.UserContextInterfaces;
using Microsoft.EntityFrameworkCore;
using System.Text;

namespace BusinessProcesses.Services.BusinessProcessServices.BusinessEntityServices
{
    public class BusinessEntityService : IBusinessEntityService
    {
        private readonly IUserContext _userContext;
        public readonly IAppRepository _repository;
        private readonly IMapper _mapper;
        //private readonly IIdentityService _identityService;
        private readonly string ERROR_MESSEGE = "Error was occured";
        private readonly string ERROR_ENTITY_NOT_FOUND = "Entity not found";
        private readonly string TEXT_WITH_COMMENT = "with comment - ";
        private readonly string ADD_ENTITY_HISTORY_TEXT = "The entity was added by the user ";
        private readonly string DELETE_ENTITY_HISTORY_TEXT = "The entity was deleted by the user ";
        private readonly string RESTORE_ENTITY_HISTORY_TEXT = "The entity was restored by the user ";
        private readonly string UPDATE_ENTITY_HISTORY_TEXT = "The entity was modified by the user ";

        public BusinessEntityService(IUserContext userContext,
                                     IAppRepository repository,
                                     IMapper mapper)
                                     //IIdentityService identityService)
        {
            _userContext = userContext;
            _repository = repository;
            _mapper = mapper;
            //_identityService = identityService;
        }

        #region Main methods
        public async Task<IAPIResponse<BusinessEntityDTO>> Add(BusinessEntityDTO entityDto)
        {
            try
            {
                var entity = _mapper.Map<BusinessEntity>(entityDto);

                entity.UpdateDate = DateTime.Now;
                if (entity.Cost <= 0) entity.Cost = 0;

                //delete all previos object history
                if (entity.BusinessEntityHistory.Any())
                    entity.BusinessEntityHistory.Clear();

                var newHistoryRow = await GetHistory(ADD_ENTITY_HISTORY_TEXT, entity.Id, null);
                entity.BusinessEntityHistory.Add(newHistoryRow);

                await _repository.AddAsync(entity);
                await _repository.SaveChangesAsync();

                var response = _mapper.Map<BusinessEntityDTO>(entity);

                return await APIResponse<BusinessEntityDTO>.SuccessAsync(response);
            }
            catch (Exception ex)
            {
                return await APIResponse<BusinessEntityDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<BusinessEntityDTO>> Update(BusinessEntityDTO entityDto)
        {
            try
            {
                var entity = _mapper.Map<BusinessEntity>(entityDto);
                var response = new BusinessEntityDTO();
                var existingAsset = await GetById(entity.Id);
                var checkChanges = CompareObjectHelper.GetCompare(entityDto, existingAsset.Data);

                if (!checkChanges)
                {
                    entity.UpdateDate = DateTime.Now;

                    var newHistoryRow = await GetHistory(UPDATE_ENTITY_HISTORY_TEXT, entity.Id, null);
                    entity.BusinessEntityHistory.Add(newHistoryRow);

                    //var updatedCollections = AssetCollectionsForNotUpdate.GetAssetNotUpdatedCollections();
                    //_repository.UpdateCollectionsNew(asset, updatedCollections);

                    _repository.Update(entity);
                    await _repository.SaveChangesAsync();

                    response = _mapper.Map<BusinessEntityDTO>(entity);

                }
                else response = entityDto;

                return await APIResponse<BusinessEntityDTO>.SuccessAsync(response);
            }
            catch//(Exception ex)
            {
                return await APIResponse<BusinessEntityDTO>.FailureAsync(ERROR_MESSEGE); ;
            }
        }

        public async Task<IAPIResponse<bool>> Delete(int id)
        {
            try
            {
                var entity = await _repository.GetAllQuery<BusinessEntity>()
                    .AsSplitQuery()
                    .Include(i => i.BusinessEntityHistory)
                    .SingleOrDefaultAsync(a => a.Id == id);


                if (entity == null)
                    return await APIResponse<bool>.FailureAsync(ERROR_ENTITY_NOT_FOUND);

                entity.IsDeleted = true;
                entity.DeletionDate = DateTime.Now;

                var newHistoryRow = await GetHistory(DELETE_ENTITY_HISTORY_TEXT, entity.Id, null);
                entity.BusinessEntityHistory.Add(newHistoryRow);

                _repository.Update(entity);
                await _repository.SaveChangesAsync();

                return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<PagedResponse<List<BusinessEntityDTO>>> GetAll(PaginationFilter filter)
        {
            try
            {
                var query = _repository.GetAllQuery<BusinessEntity>();

                if (!string.IsNullOrEmpty(filter.Search))
                {
                    var search = filter.Search.Trim().ToLower();
                    query = query.Where(e => (e.Name != null) && e.Name.ToLower()
                                 .Contains(search));
                }

                var count = await query.CountAsync();

                query = query.OrderBy(o => o.UpdateDate);

                var filteredDate = query.Skip((filter.PageNumber - 1) * filter.PageSize)
                                   .Take(filter.PageSize);

                var result = _mapper.Map<IEnumerable<BusinessEntityDTO>>(filteredDate).ToList();
                return new PagedResponse<List<BusinessEntityDTO>>(result, count, filter.PageSize);
            }
            catch (Exception ex)
            {
                return (PagedResponse<List<BusinessEntityDTO>>)await APIResponse<List<BusinessEntityDTO>>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<BusinessEntityDTO>> GetById(int id)
        {
            try
            {
                var entity = await _repository
                    .GetAllQuery<BusinessEntity>()
                    .AsSplitQuery()
                    .Include(i => i.BusinessEntityHistory)
                    .Include(i => i.AssetSubTypeXAssetAttributes).ThenInclude(t => t.BusinessEntityAttributes)
                    .AsNoTracking()
                    .SingleOrDefaultAsync(a => a.Id == id);

                //asset.AssetXRelatedAssets = await _repository.GetQuery<AssetXRelatedAsset>().Where(w => w.CurrentAssetId == Id).ToListAsync();

                var response = _mapper.Map<BusinessEntityDTO>(entity);
                return await APIResponse<BusinessEntityDTO>.SuccessAsync(response);
            }
            catch (Exception ex)
            {
                return await APIResponse<BusinessEntityDTO>.FailureAsync(ERROR_MESSEGE);
            }
        }

        public async Task<IAPIResponse<bool>> Restore(int id)
        {
            try
            {
                var entity = await _repository.GetAllQuery<BusinessEntity>()
                    .AsSplitQuery()
                    .Include(i => i.BusinessEntityHistory)
                    .SingleOrDefaultAsync(a => a.Id == id);


                if (entity == null)
                    return await APIResponse<bool>.FailureAsync(ERROR_ENTITY_NOT_FOUND);

                entity.IsDeleted = false;
                entity.DeletionDate = null;

                var newHistoryRow = await GetHistory(RESTORE_ENTITY_HISTORY_TEXT, entity.Id, null);
                entity.BusinessEntityHistory.Add(newHistoryRow);

                _repository.Update(entity);
                await _repository.SaveChangesAsync();

                return await APIResponse<bool>.SuccessAsync();
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }



        public async Task<IAPIResponse<bool>> CheckEntity(int entityId)
        {
            try
            {
                var entity = await _repository.GetByIdAsync<BusinessEntity>(entityId);
                if (entity.IsActive)
                    return await APIResponse<bool>.SuccessAsync("Is active");
                else
                    return await APIResponse<bool>.SuccessAsync("Is not active");
            }
            catch (Exception ex)
            {
                return await APIResponse<bool>.FailureAsync(ERROR_MESSEGE);
            }
        }
        #endregion


        #region Support methods
        public async Task<BusinessEntityHistory> GetHistory(string HistoryText, int entityId, string? comment)
        {

            comment = comment != null ? TEXT_WITH_COMMENT + comment : string.Empty;
            StringBuilder @string = new StringBuilder();
            var actionType = @string.Append(HistoryText)
                                    .Append(_userContext.CurrentUserId)
                                    .Append(comment).ToString();
            var newHistory = new BusinessEntityHistory()
            {
                ActionDate = DateTime.Now,
                ActionType = actionType,
                BusinessEntityId = entityId
            };
            return newHistory;
        }
        #endregion


    }
}
