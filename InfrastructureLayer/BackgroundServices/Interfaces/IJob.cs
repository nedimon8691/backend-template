﻿namespace BackgroundServices.Interfaces
{
    public interface IJob
    {
        Task Process(int jobId);
    }
}
