﻿using BackgroundServices.Interfaces;
using Core.DTOModels.BackgroundServicesDTO;

namespace BackgroundServices.Services
{
    public interface IScheduleJobManager<T> where T : IJob
    {
        void AddJob(string jobName, int jobId, int interval, IntervalEnum intervalType);

        void UpdateJob(string oldJobName, string jobName, int jobId, int interval, IntervalEnum intervalType);
        void DeleteJob(string jobName);

    }
}
