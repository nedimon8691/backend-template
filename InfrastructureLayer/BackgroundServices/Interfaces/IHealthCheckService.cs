﻿namespace BackgroundServices.Interfaces
{
    public interface IHealthCheckService
    {
        Task<bool> Check(string url);
    }
}
