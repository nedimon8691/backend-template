﻿using BackgroundServices.Interfaces;
using BackgroundServices.Services;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace BackgroundServices.BackGroundSettings
{
    public static class HangFireConfigurationExtension
    {
        public static void AddBackgroundServices(this IServiceCollection services, string connectionString)
        {
            //services.AddDbContext<BackgroundDbContext>(options =>
            //{
            //    options.UseSqlServer(connectionString);
            //});

            services.AddTransient<IJobsRepository, JobsRepository>();
            services.AddTransient<IJobService, JobService>();
            services.AddSingleton<IScheduleJobManager<IHealthCheckJobManager>, ScheduleJobManager<IHealthCheckJobManager>>();
            services.AddTransient<IHealthCheckJobManager, HealthCheckJobManager>();
            services.AddHttpClient<IHealthCheckService, HealthCheckService>(client =>
            {
                client.Timeout = TimeSpan.FromMilliseconds(10000);
            });

            services.AddHangfire(config =>
            {
                var options = new SqlServerStorageOptions
                {
                    PrepareSchemaIfNecessary = true,
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true
                };
                config.UseSqlServerStorage(connectionString).WithJobExpirationTimeout(TimeSpan.FromHours(1));
            });

            services.AddHangfireServer();
            GlobalJobFilters.Filters.Add(new AutomaticRetryAttribute { Attempts = 1 }); //change - from 7 to 1

        }


        public static void UseHangFireDashboard(this IApplicationBuilder app)
        {
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() }
            });
        }

    }
}
