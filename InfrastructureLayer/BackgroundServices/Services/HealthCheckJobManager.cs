﻿using BackgroundServices.Interfaces;

namespace BackgroundServices.Services
{
    public class HealthCheckJobManager : IHealthCheckJobManager
    {
        private readonly IJobsRepository _jobRepository;
        private readonly IHealthCheckService _healthCheckerService;
        public HealthCheckJobManager(IJobsRepository jobRepository, IHealthCheckService healthCheckerService)
        {

            _jobRepository = jobRepository;
            _healthCheckerService = healthCheckerService;
        }

        public async Task Process(int jobId)
        {
            var job = await _jobRepository.GetJob(jobId);
            if (job == null)
            {
                return;
            }
            else
            {
                var url = job.TargetURL;
                var healtResult = await _healthCheckerService.Check(job.TargetURL);

            }
            job.LastRunTime = DateTime.Now;
            await _jobRepository.UpdateJob(job, "");
        }
    }
}
