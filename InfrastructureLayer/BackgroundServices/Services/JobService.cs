﻿using Core.DTOModels.APIRequestResponseDTOModels.ResponseDTO;
using Core.DTOModels.BackgroundServicesDTO;

namespace BackgroundServices.Services
{
    public class JobService : IJobService
    {
        private readonly IScheduleJobManager<IHealthCheckJobManager> _scheduleManager;
        private readonly IJobsRepository _jobRepository;
        private readonly string ERROR_MESSEGE = "Error was occured";

        public JobService(IScheduleJobManager<IHealthCheckJobManager> scheduleManager, IJobsRepository jobsRepository)
        {
            _scheduleManager = scheduleManager;
            _jobRepository = jobsRepository;
        }

        public async Task<IAPIResponse<JobDTO>> AddJob(JobRequestDTO job, string userName)
        {
            try
            {
                var result = await _jobRepository.AddJob(job, userName);
                var jobName = await CreateUniqueJobName(result.Name, userName);
                _scheduleManager.AddJob(jobName, result.Id, result.TriggerInterval, result.TriggerType);
                return await APIResponse<JobDTO>.SuccessAsync(result);
            }
            catch
            {
                return await APIResponse<JobDTO>.FailureAsync(ERROR_MESSEGE);
            }


        }

        public async Task<IAPIResponse<bool>> DeleteJob(int id, string userName)
        {
            var job = await _jobRepository.GetJob(id);
            string jobName = job.Name;

            var result = await _jobRepository.DeleteJob(id, userName);
            _scheduleManager.DeleteJob(jobName);

            return await APIResponse<bool>.SuccessAsync(result);
        }

        public async Task<IAPIResponse<JobDTO>> GetJob(int id)
        {
            var result = await _jobRepository.GetJob(id);

            return await APIResponse<JobDTO>.SuccessAsync(result);
        }

        public async Task<IAPIResponse<List<JobDTO>>> GetJobs(string userName)
        {
            var result = await _jobRepository.GetJobs(userName);

            return await APIResponse<List<JobDTO>>.SuccessAsync(result);
        }

        public async Task<IAPIResponse<JobDTO>> UpdateJob(JobDTO job, string userName)
        {

            var oldJob = await _jobRepository.GetJob(job.Id);
            string oldJobName = oldJob.Name;

            var result = await _jobRepository.UpdateJob(job, userName);

            var jobName = await CreateUniqueJobName(result.Name, userName);
            //await CreateUniqueJobName(result.Name)
            _scheduleManager.UpdateJob(jobName, jobName, result.Id, result.TriggerInterval, result.TriggerType);

            return await APIResponse<JobDTO>.SuccessAsync(result);
        }

        private async Task<string> CreateUniqueJobName(string jobName, string userName)
        {
            //var user = await _userRepository.GetUserByName(userName);

            return $"{jobName}-{userName}";
        }

    }
}
