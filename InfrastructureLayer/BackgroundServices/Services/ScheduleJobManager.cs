﻿using BackgroundServices.Interfaces;
using Core.DTOModels.BackgroundServicesDTO;
using Hangfire;

namespace BackgroundServices.Services
{
    public class ScheduleJobManager<T> : IScheduleJobManager<T> where T : IJob
    {
        public void AddJob(string jobName, int jobId, int interval, IntervalEnum intervalType)
        {
            RecurringJob.AddOrUpdate<T>(jobName, j => j.Process(jobId), GetCronFromInterval(interval, intervalType));
        }

        public void DeleteJob(string jobName)
        {
            RecurringJob.RemoveIfExists(jobName);
        }

        public void UpdateJob(string oldJobName, string jobName, int jobId, int interval, IntervalEnum intervalType)
        {
            RecurringJob.RemoveIfExists(oldJobName);
            RecurringJob.AddOrUpdate<T>(jobName, j => j.Process(jobId), GetCronFromInterval(interval, intervalType));
        }

        private string GetCronFromInterval(int interval, IntervalEnum intervalType)
        {
            var result = intervalType switch
            {
                IntervalEnum.Minute => Cron.MinuteInterval(interval),
                IntervalEnum.Hour => Cron.HourInterval(interval),
                IntervalEnum.Day => Cron.DayInterval(interval),
                IntervalEnum.Week => Cron.Weekly(),
                IntervalEnum.Month => Cron.MonthInterval(interval),
                _ => throw new NotImplementedException()
            };
            return result;
        }
    }
}
