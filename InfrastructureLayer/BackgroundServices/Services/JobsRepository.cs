﻿using AutoMapper;
using BackgroundServices.Models;
using Core.DTOModels.BackgroundServicesDTO;
using Core.Interfaces.RepositoryInterfaces;
using Microsoft.AspNetCore.Identity;

namespace BackgroundServices.Services
{
    public class JobsRepository : IJobsRepository
    {
        //private readonly BackgroundDbContext _context;
        private readonly IBackgroundRepository _repository;
        private readonly IMapper _mapper;

        public JobsRepository(IBackgroundRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<JobDTO> AddJob(JobRequestDTO job, string userName)
        {
            //var jobEntity = _mapper.Map<Job>(job);

            //var user = await _context.Users.FirstAsync(u => u.UserName == userName);

            //jobEntity.User = user;
            try
            {
                if (job != null)
                {
                    var jobEntity = new Job();
                    jobEntity.Created = DateTime.Now;
                    jobEntity.CreatedUser = userName;
                    jobEntity.LastEditUser = userName;
                    jobEntity.TriggerInterval = job.TriggerInterval;
                    jobEntity.TriggerType = job.TriggerType;
                    jobEntity.TargetURL = job.TargetURL;
                    jobEntity.Name = job.Name;
                    //await _context.Jobs.AddAsync(jobEntity);
                    await _repository.AddJob<Job>(jobEntity);
                    _repository.SaveChanges();

                    //var result = await _context.SaveChangesAsync();

                    //if (result > 0)
                    //{
                    //    return _mapper.Map<JobDTO>(jobEntity);
                    //}
                }
            }
            catch
            {
                throw new Exception("Job could not be saved to db ");
            }


            throw new Exception("Job could not be saved to db ");

        }


        public async Task<JobDTO> UpdateJob(JobDTO job, string userName)
        {
            IdentityUser _user = null;

            //var jobEntity = await _context.Jobs.AsNoTracking().FirstAsync(j => j.Id == job.Id);
            var jobEntity = await _repository.GetJob<Job>(job.Id);

            if (jobEntity == null)
            {
                throw new Exception("Job could not be found to update ");
            }

            jobEntity.Name = job.Name;
            jobEntity.TargetURL = job.TargetURL;
            jobEntity.TriggerInterval = job.TriggerInterval;
            jobEntity.TriggerType = job.TriggerType;

            jobEntity.CreatedUser = _user != null ? _user.UserName : jobEntity.CreatedUser;
            jobEntity.LastEditUser = _user != null ? _user.UserName : jobEntity.LastEditUser;
            jobEntity.LastEdit = DateTime.Now;
            jobEntity.LastRunTime = job.LastRunTime;

            _repository.UpdateJob(job);
            _repository.SaveChanges();
            return _mapper.Map<JobDTO>(jobEntity);
            //_context.Update(jobEntity);
            //var result = await _context.SaveChangesAsync();

            //if (result > 0)
            //{
            //    return _mapper.Map<JobDTO>(jobEntity);
            //}

            //throw new Exception("Job could not be updated");
        }


        public async Task<JobDTO> GetJob(int id)
        {
            //var job = await _context.Jobs.AsNoTracking()
            //                                 .Where(j => j.Id == id)
            //                                 .Select(j => _mapper.Map<JobDTO>(j))
            //                                 .FirstOrDefaultAsync();
            var job = await _repository.GetJob<Job>(id);
            if (job == null)
            {
                throw new Exception($"There is no job found with id - {id}");
            }

            return _mapper.Map<JobDTO>(job);
        }


        public async Task<List<JobDTO>> GetJobs(string userName)
        {

            //var user = await _context.Users.FirstAsync(u => u.UserName == userName);

            //var jobs = await _context.Jobs
            //    .AsNoTracking()
            //    .Where(j => /*j.UserId == user.Id && */j.Tombstone == null)
            //    .Select(j => _mapper.Map<JobDTO>(j))
            //    .ToListAsync();
            var jobs = _repository.GetJobs<Job>();
            var jobsDto = _mapper.Map<List<JobDTO>>(jobs);
            return jobsDto;
        }

        public async Task<bool> DeleteJob(int id, string userName)
        {
            //var user = await _context.Users.FirstAsync(u => u.UserName == userName);

            //var foundJobEntity = await _context.Jobs.FirstOrDefaultAsync(j => j.Id == id);
            var foundJobEntity = await _repository.GetJob<Job>(id);

            if (foundJobEntity == null)
                throw new Exception($"Job was not found.");

            foundJobEntity.Tombstone = DateTime.Now;
            foundJobEntity.LastEditUser = userName;
            foundJobEntity.LastEdit = DateTime.Now;
            _repository.UpdateJob(foundJobEntity);
            _repository.SaveChanges();
            return true;
            //_context.Update(foundJobEntity);
            //var result = await _context.SaveChangesAsync();

            //if (result > 0)
            //    return true;

            //return false;
        }
    }
}
