﻿using AutoMapper;
using BackgroundServices.Models;
using Core.DTOModels.BackgroundServicesDTO;

namespace BackgroundServices.Mapping
{
    public class BackgroundMappingProfile : Profile
    {
        public BackgroundMappingProfile()
        {
            CreateMap<JobDTO, Job>().ReverseMap();
        }
    }
}
