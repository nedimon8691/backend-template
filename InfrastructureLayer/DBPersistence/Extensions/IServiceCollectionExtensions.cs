﻿using BusinessProcesses.Models.UserModels;
using Core.Interfaces.AuditModuleInterfaces;
using Core.Interfaces.RepositoryInterfaces;
using DBPersistence.Data.DBContext;
using DBPersistence.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DBPersistence.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static void AddPersistenceLayer(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDBContext(configuration);
            services.AddBackgroundDbContext(configuration);
            services.AddServices();
        }


        public static void AddDBContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });

            services.AddIdentity<ApplicationUser, ApplicationRole>(options =>
            {
                options.User.RequireUniqueEmail = false;
            })
            .AddEntityFrameworkStores<AppDbContext>();
        }

        public static void AddBackgroundDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("BackGroundDbConnection");
            services.AddDbContext<BackgroundDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
        }
        public static void AddAuditDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("AuditConnection");
            services.AddDbContext<AuditDBContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });
        }



        public static void AddServices(this IServiceCollection services)
        {
            services
                .AddTransient<IAppRepository, AppRepository>()
                .AddTransient<IAuditDBRepository, AuditDBRepository>()
                .AddTransient<IBackgroundRepository, BackgroundRepository>();
            //.AddTransient(typeof(IGenericRepository), typeof(GenericRepository<>));
        }
    }
}
