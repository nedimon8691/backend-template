﻿using AuditDBPersistence.Models.ObjectChangesData;
using AuditModule.Models.ApplicationErrorsData;
using AuditModule.Models.OperationCallingData;
using Microsoft.EntityFrameworkCore;

namespace DBPersistence.Data.DBContext
{
    public class AuditDBContext : DbContext
    {
        public AuditDBContext(DbContextOptions<AuditDBContext> options) : base(options)
        {

        }

        public DbSet<SystemErrorsAuditData> SystemErrorsAuditDatas { get; set; }
        public DbSet<CallingOperationsByUser> CallingOperationsByUsers { get; set; }
        public DbSet<BusinessEntityChange> AuditAssetChanges { get; set; }
    }
}
