﻿using BusinessProcesses.Models.BusinessProcessesModels.BusinessEntityModels;
using BusinessProcesses.Models.UserModels;
using Core.Models.ApplicationModels;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace DBPersistence.Data.DBContext
{
    public class AppDbContext : IdentityDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<BusinessEntity> BusinessEntities { get; set; }
        public DbSet<BusinessEntityType> BusinessEntityTypes { get; set; }
        public DbSet<BusinessEntityHistory> BusinessEntityHistories { get; set; }
        public DbSet<BusinessEntityAttributes> BusinessEntityAttributes { get; set; }
        public DbSet<BusinessEntityXBusinessEntityAttributes> BusinessEntityXBusinessEntityAttributes { get; set; }
        public DbSet<ApplicationMethods> ApplicationMethods { get; set; }

    }
}
