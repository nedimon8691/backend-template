﻿using BackgroundServices.Models;
using Microsoft.EntityFrameworkCore;

namespace DBPersistence.Data.DBContext
{
    public class BackgroundDbContext : DbContext
    {
        public BackgroundDbContext(DbContextOptions<BackgroundDbContext> options) : base(options)
        {
        }
        public DbSet<Job> Jobs { get; set; }
    }
}
