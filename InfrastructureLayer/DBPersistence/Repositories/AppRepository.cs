﻿using Core.Interfaces.RepositoryInterfaces;
using DBPersistence.Data.DBContext;
using Microsoft.EntityFrameworkCore;

namespace DBPersistence.Repositories
{
    public class AppRepository : IAppRepository//<T> where T : class
    {
        private readonly AppDbContext _dbContext;

        public AppRepository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<T> AddAsync<T>(T entity) where T : class
        {
            await _dbContext.Set<T>().AddAsync(entity);
            return entity;
            //throw new NotImplementedException();
        }

        public async Task<List<T>> AddRangeAsync<T>(List<T> entities) where T : class
        {
            await _dbContext.Set<T>().AddRangeAsync(entities);
            return entities;
        }

        public void Update<T>(T entity) where T : class
        {
            _dbContext.Set<T>().Update(entity);
        }

        public Task DeleteFromDBAsync<T>(T entity) where T : class
        {
            //var entity = await _dbContext.Set<T>().FindAsync(id);
            _dbContext.Remove(entity);
            return Task.CompletedTask;
        }

        public Task DeleteFromDBAsyncRange<T>(IEnumerable<T> entities) where T : class
        {
            _dbContext.RemoveRange(entities);
            return Task.CompletedTask;
        }
        //TO=DO rename
        public async Task<List<T>> GetAllAsync<T>() where T : class
        {
            return await _dbContext.Set<T>().ToListAsync();
        }

        public IQueryable<T> GetAllQuery<T>() where T : class
        {
            return _dbContext.Set<T>();
        }

        public async Task<T> GetByIdAsync<T>(int id) where T : class
        {
            return await _dbContext.Set<T>().FindAsync(id);
        }

        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken)
        {
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
