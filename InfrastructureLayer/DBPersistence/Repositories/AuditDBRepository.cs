﻿using Core.Interfaces.AuditModuleInterfaces;
using DBPersistence.Data.DBContext;
using Microsoft.EntityFrameworkCore;

namespace DBPersistence.Repositories
{
    public class AuditDBRepository : IAuditDBRepository
    {
        private readonly AuditDBContext _context;

        public AuditDBRepository(AuditDBContext context)
        {
            _context = context;
        }

        public async Task<T> Add<T>(T entity) where T : class
        {
            await _context.Set<T>().AddAsync(entity);
            return entity;
        }

        public async void Delete<T>(int id) where T : class
        {
            var entity = await _context.FindAsync<T>(id);
            _context.Remove(entity);
        }

        public async Task<T> Get<T>(int id) where T : class
        {
            return await _context.FindAsync<T>(id);
        }

        public async Task<List<T>> GetAll<T>() where T : class
        {
            return await _context.Set<T>().ToListAsync();
        }

        public async void SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public async Task<T> Update<T>(T entity) where T : class
        {
            _context.Set<T>().Update(entity);
            return entity;
        }
    }
}
