﻿using Core.Interfaces.RepositoryInterfaces;
using DBPersistence.Data.DBContext;

namespace DBPersistence.Repositories
{
    public class BackgroundRepository : IBackgroundRepository
    {
        private readonly BackgroundDbContext _context;

        public BackgroundRepository(BackgroundDbContext context)
        {
            _context = context;
        }

        public async Task<T> AddJob<T>(T entity) where T : class
        {
            await _context.Set<T>().AddAsync(entity);
            return entity;
        }

        public async void DeleteJob(int id)
        {
            var job = await _context.Jobs.FindAsync(id);
            _context.Remove(job);
        }

        public async Task<T> GetJob<T>(int id) where T : class
        {
            return await _context.FindAsync<T>(id);
        }

        public List<T> GetJobs<T>() where T : class
        {
            return _context.Set<T>().ToList();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public void UpdateJob<T>(T job) where T : class
        {
            _context.Set<T>().Update(job);
        }
    }
}
