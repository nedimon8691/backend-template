﻿namespace MailNotification.MailTemplates
{
    public static class MailTemplate
    {
        public static string GetMailBody(string message)
        {
            string css_style = @"<style>
                            body {
                                margin: 0;
                                padding: 0;
                                background-color: #fff;
                                color: #000;
                                font-family: Arial,Helvetica,sans-serif;
                                font-size: 9.5pt;
                                line-height: 12pt;
                            }

                            table {
                                margin: 0;
                                padding: 0;
                                border: 0;
                                border-collapse: collapse;
                                table-layout: fixed;
                            }

                            .fw {
                                width: 100%;
                            }

                            td {
                                padding: 0;
                                vertical-align: top;
                            }

                            .pad-h {
                                padding: 0 25px;
                            }

                            .pad-v {
                                padding: 15px 0;
                            }

                            #lt {
                                width: auto;
                            }

                            #ve {
                                width: 5.5%;
                            }

                            #msg {
                                width: 566px;
                            }

                            #logo {
                                padding: 30px 25px 15px;
                            }

                            #fbox {
                                border-top: 1px solid #999;
                            }

                                #fbox p {
                                    color: #747678;
                                    font-size: 8pt;
                                    line-height: 10pt;
                                    margin: 0 0 10pt;
                                }

                            h1 {
                                color: #00338d;
                                font-size: 22pt;
                                line-height: 26pt;
                                margin: 0 0 15px;
                            }

                            h2 {
                                color: #0091da;
                                font-size: 12pt;
                                line-height: 14pt;
                                margin: 0 0 8pt;
                            }

                            h3 {
                                color: #483698;
                                font-size: 10.5pt;
                                line-height: 12.5pt;
                                margin: 0 0 10pt;
                            }

                            h4 {
                                color: #0091da;
                                font-size: 9.5pt;
                                font-style: normal;
                                font-weight: normal;
                                line-height: 12pt;
                                margin: 0 0 8pt;
                            }

                            small {
                                font-weight: normal;
                            }

                            p {
                                font-size: 9.5pt;
                                line-height: 12pt;
                                margin: 0 0 9.5pt;
                            }

                            ul, ol {
                                font-size: 9.5pt;
                                line-height: 11.5pt;
                                margin: 0 0 11.5pt 18pt;
                                padding: 0;
                            }

                            li {
                                margin: 0 0 4pt;
                            }
                            a, a:link, a:visited {
                                color: #005eb8;
                                text-decoration: underline;
                            }
                                a.flnk {
                                    color: #747678;
                                    text-decoration: none;
                                }
                            hr {
                                border-bottom: 0;
                                border-top: 1px solid #ccc;
                                margin: 15px 0;
                            }

                            @media only screen and (max-width:640px) {
                                #msg {
                                    width: 100% !important;
                                }

                                h1 {
                                    font-size: 20pt;
                                    line-height: 24pt;
                                }
                            }
                            .greeting {
                                font-size: 16px;
                            }
                            .date {
                                color: #0091da;
                                font-size: 16px;
                            }
                            .gutter {
                                height: 15px;
                                width: 15px;
                            }
                            #spacer {
                                padding: 4px;
                            }
                            .callout {
                                padding: 15px;
                                border: 1px solid #0091da;
                            }

                            a.ttl {
                                color: #000;
                            }

                            .data-table {
                                width: 100%;
                            }

                                .data-table td {
                                    padding: 4px;
                                    border: 1px solid #ddd;
                                }

                            .list-table {
                                width: 100%;
                            }

                                .list-table td {
                                    padding: 4px 8px 4px 0;
                                }

                            .gap-right { margin-right: 24px; }
                            .gap-bottom { margin-bottom: 24px; }

                            .btn, a.btn {
                                display: inline-block;
                                line-height: 1;
                                font-size: 12px;
                                font-family: Arial,Helvetica,sans-serif;
                                font-weight: normal;
                                text-align: center;
                                text-indent: initial;
                                text-transform: uppercase;
                                margin: 0;
                                padding: 8px 12px;
                                background-image: none;
                                white-space: nowrap;
                                overflow: visible;
                                vertical-align: middle;
                                cursor: pointer;
                            }

                                .btn a, a.btn {
                                    text-decoration: none;
                                }

                                .btn td {
                                    padding: 5px;
                                }

                            .btn-blue {
                                border: 1px solid #005eb8;
                            }

                            .btn-grey {
                                border: 1px solid #ccc;
                            }

                                .btn-grey a{
                                    color: #333;
                                }

                            .btn-green {
                                border: 1px solid #009a44;
                            }

                                .btn-green a  {
                                    color: #009a44;
                                }

                            .btn-red {
                                border: 1px solid #bc204b;
                            }

                                .btn-red a {
                                    color: #bc204b;
                                }

                            .bg1 {
                                background: #00338d;
                            }

                            .bg2 {
                                background: #005eb8;
                            }

                            .bg3 {
                                background: #0091da;
                            }

                            .bg4 {
                                background: #483698;
                            }

                            .bg5 {
                                background: #470a68;
                            }

                            .bg6 {
                                background: #6d2077;
                            }

                            .bg7 {
                                background: #00a3a1;
                            }


                            .bg8 {
                                background: #009a44;
                            }

                            .bg9 {
                                background: #43b02a;
                            }

                            .bg12 {
                                background: #bc204b;
                            }

                            .clrw {
                                color: #fff;
                            }

                            .clrb {
                                color: #000;
                            }

                            .clrg {
                                color: #333;
                            }

                            .clr1 {
                                color: #00338d;
                            }

                            .clr2 {
                                color: #005eb8;
                            }

                            .clr3 {
                                color: #0091da;
                            }

                            .clr4 {
                                color: #483698;
                            }

                            .clr5 {
                                color: #470a68;
                            }

                            .clr6 {
                                color: #6d2077;
                            }

                            .clr7 {
                                color: #00a3a1;
                            }

                            .clr8 {
                                color: #009a44;
                            }

                            .clr9 {
                                color: #43b02a;
                            }

                            .clr10 {
                                color: #eaaa00;
                            }

                            .clr11 {
                                color: #f68d2e;
                            }

                            .clr12 {
                                color: #bc204b;
                            }

                            .clr13 {
                                color: #c6007e;
                            }

                            .cdn {
                                margin: 0 0 2pt;
                                font-weight: bold;
                            }

                            .cdd {
                                font-size: 8pt;
                                line-height: 10pt;
                                margin: 0;
                            }
                        </style>

                        <style>
                            .ct {
                                width: 100%;
                                font-size: 11px;
                            }

                                .ct td,
                                .ct th {
                                    padding: 2px 4px;
                                    border: 1px solid #999;
                                }

                                .ct th {
                                    background-color: #f1f1f1;
                                    text-align: left;
                                }

                            /*history table*/
                            .history {
                                width: 100%;
                            }

                                .history th {
                                    background-color: #e6eff6;
                                    padding: 2px 4px;
                                }

                                .history td {
                                    padding: 2px 4px;
                                    border-bottom: 0.25pt dotted #6E8D24;
                                }

                            .properties {
                                width: 100%;
                            }

                                .properties th {
                                    width: 30%;
                                    padding: 3px 6pt 3pt 0;
                                    text-align: left;
                                    vertical-align: top;
                                    font-weight: normal;
                                    border-bottom: 0.25pt dotted #6E8D24;
                                }

                                .properties td {
                                    width: 70%;
                                    padding: 3px 6pt;
                                    vertical-align: top;
                                    border-bottom: 0.25pt dotted #6E8D24;
                                }
                        </style>";
            string link = "";

            string body = @"<!DOCTYPE html><html xmlns='http://www.w3.org/1999/xhtml'><head>
                    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
                        <meta name='viewport' content='width=device-width, initial-scale=1'>
                        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                        <meta name='format-detection' content='telephone=no'>
                        <title>Mail notification</title>
                        " + css_style +
        @"</head>

                    <body>
                        <table id='lt'>
                            <tr>
                                <td id='ve' class='bg1'><table><tr><td id='spacer'><a name='_top' id='_top'></a>&nbsp;</td></tr></table></td>
                                <td id='msg'>
                                    <table class='fw'>
                                        <!--Header-->

                                        <!--Body-->
                                        <tr>
                                            <td class='pad-h'>

                                                <table class='fw'>
                                                    <tr>
                                                        <td class='pad-v'>
                                                            <!--START: Message content-->
                                                                <h1>
                                                                    System notification
                                                                </h1>     

                                                                <p>Message title</p>

                                                                <p>" + message + @"</p>"
                                                + link + @"

                                                            <!--END: Message content-->
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <!--Related Sites-->
                                        <!--Footer-->
                                        <tr>
                                            <td class='pad-h'>
                                                <table class='fw'>
                                                    <tr>
                                                        <td class='pad-v' id='fbox'>


                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </body>
                    </html>";

            return body;
        }
    }
}
