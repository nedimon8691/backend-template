﻿namespace MailNotification.Models
{
    public class MailNotificationSettings
    {
        public string Server { get; set; }
        public int Port { get; set; }
        public string Domain { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

    }
}
