﻿using Core.Interfaces.NotificationsInterfaces;
using MailNotification.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MailNotification.Extensions
{
    public static class IServiceCollectionExtenions
    {
        public static void AddMailNotification(this IServiceCollection services)
        {
            services.AddServices();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IMailNotificationService, MailNotificationService>();
        }
    }
}
