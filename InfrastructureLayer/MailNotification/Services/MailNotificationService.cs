﻿using Core.Interfaces.NotificationsInterfaces;
using Core.Models.AppsettingsModels;
using MailNotification.MailTemplates;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;

namespace MailNotification.Services
{
    internal class MailNotificationService : IMailNotificationService
    {
        public IConfiguration _configuration;

        public MailNotificationService(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task SendEmailAsync(string subject, List<string> receiverEmails, string body, byte[] file)
        {
            try
            {
                if (receiverEmails.Any())
                {
                    var appSettingsSection = _configuration.GetSection("MailNotificationSettings");
                    var settings = appSettingsSection.Get<MailNotificationSettings>();
                    var userName = settings.Username;
                    var password = settings.Password;
                    using var smtpClient = new SmtpClient(settings.Server)
                    {
                        Credentials = new NetworkCredential(userName, password),
                        Port = Convert.ToInt32(settings.Port) //TODO
                    };
                    smtpClient.EnableSsl = true;
                    using var message = new MailMessage
                    {
                        From = new MailAddress(settings.Username),
                        Subject = subject,
                        Body = MailTemplate.GetMailBody(subject),
                        IsBodyHtml = true,
                    };

                    foreach (var receiverEmail in receiverEmails)
                    {
                        message.To.Add(receiverEmail);
                    }


                    smtpClient.SendCompleted += (sender, e) =>
                    {
                    };
                    smtpClient.Send(message);
                }

            }
            catch (Exception ex)
            {
                //DO Something if error was occured
                throw;
            }
        }
    }
}
